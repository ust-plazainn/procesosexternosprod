package com.econtract;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class AccesoDBParam
  implements Serializable
{
  private static Map dbParams = new HashMap();
  
  static
  {
    System.out.println("Cargando Parametros de BD");
    cargaDBParametros();
  }
  
  public static void cargaDBParametros()
  {
    try
    {
      ResourceBundle bundle = ResourceBundle.getBundle("accesoDBParam");
      Enumeration enume = bundle.getKeys();
      while (enume.hasMoreElements())
      {
        String key = (String)enume.nextElement();
        String objeto = (String)bundle.getObject(key);
        if (objeto != null) {
          objeto = objeto.trim();
        }
        dbParams.put(key, objeto);
      }
    }
    catch (RuntimeException e)
    {
      System.out.println("Error al cargar los parámetros. No se puede continuar");
      
      throw e;
    }
  }
  
  public static String getParametro(String param)
  {
    if ((param == null) || (param.trim().equals("")))
    {
      System.out.println("param=" + param);
      throw new IllegalArgumentException("AccesoDBParam:getParametro():Error de parametros");
    }
    String texto = (String)dbParams.get(param);
    

    return texto;
  }
}
