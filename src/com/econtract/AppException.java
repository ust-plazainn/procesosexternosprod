package com.econtract;

public class AppException
  extends RuntimeException
{
  public AppException() {}
  
  public AppException(String mensaje)
  {
    super(mensaje);
  }
  
  public AppException(String mensaje, Throwable causaException)
  {
    super(mensaje, causaException);
  }
  
  public AppException(Throwable causaException)
  {
    super(causaException);
  }
}
