package com.econtract.data;

import com.econtract.Registros;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ArchivoExcel
{
  private ByteArrayOutputStream arci;
  
  private void $init$()
  {
    this.arci = null;
  }
  
  public ArchivoExcel(Registros reg)
    throws IOException
  {
    $init$();
    
    String strSheetName = "Resultados de hoy";
    HSSFWorkbook wb = new HSSFWorkbook();
    HSSFSheet sheet = wb.createSheet(strSheetName);
    HSSFRow row = null;
    
    HSSFCellStyle headerStyle = wb.createCellStyle();
    HSSFFont headerFont = wb.createFont();
    
    headerFont.setFontName("Tahoma");
    headerFont.setColor((short)9);
    headerFont.setBoldweight((short)2);
    headerFont.setFontHeightInPoints((short)10);
    
    headerStyle.setAlignment((short)6);
    headerStyle.setFillBackgroundColor((short)54);
    headerStyle.setFillForegroundColor((short)54);
    headerStyle.setFillPattern((short)1);
    headerStyle.setFont(headerFont);
    
    row = sheet.createRow(0);
    int numCol = 7;
    
    HSSFCell cell = null;
    
    ArrayList fields = new ArrayList();
    fields.add("RFC");
    fields.add("Doc Procesado");
    fields.add("Fecha en que se program� el Expediente");
    fields.add("Fecha inicio procesamiento del Expediente ");
    fields.add("Fecha Fin procesamiento del Expediente");
    fields.add("Tama�o");
    
    fields.add("Mensaje");
    for (short i = 0; i < fields.size(); i = (short)(i + 1))
    {
      cell = row.createCell(i);
      cell.setCellStyle(headerStyle);
      cell.setCellValue("" + fields.get(i).toString().replaceAll("_", " "));
    }
    int ind = 1;
    List datos = null;
    List renglon = null;
    datos = reg.getData();
    double mb = 0.0D;
    for (; reg.next(); ind++)
    {
      row = sheet.createRow(ind);
      
      renglon = (List)datos.get(ind - 1);
      for (int j = 0; j < numCol; j++)
      {
        cell = row.createCell((short)j);
        if (j == numCol - 2)
        {
          mb = Double.parseDouble(renglon.get(j) + "");
          cell.setCellValue(new DecimalFormat("#.##").format(mb / 1024.0D / 1024.0D) + " MB");
        }
        else
        {
          cell.setCellValue(renglon.get(j) + "");
        }
      }
    }
    ByteArrayOutputStream tempout = new ByteArrayOutputStream();
    wb.write(tempout);
    
    this.arci = tempout;
    tempout.close();
  }
  
  public void setArci(ByteArrayOutputStream arci)
  {
    this.arci = arci;
  }
  
  public ByteArrayOutputStream getArci()
  {
    return this.arci;
  }
}
