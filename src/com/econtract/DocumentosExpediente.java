package com.econtract;

import com.econtract.mail.Correo;
import com.econtract.persistence.AccesoDB;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.faceless.pdf2.PDF;
import org.faceless.pdf2.PDFAction;
import org.faceless.pdf2.PDFBookmark;
import org.faceless.pdf2.PDFPage;
import org.faceless.pdf2.PDFReader;
import org.apache.commons.logging.Log;

public class DocumentosExpediente {

	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private static final Log log = 
			ServiceLocator.getInstance().getLog(DocumentosExpediente.class);


	private static String plantillaPath;
	private static long fechaModPlantilla = 0;
	private static ArrayList listaBookmarks;
	private static List bookmarksOrdenados;
	private static Map bookmarksPDF;
	private static int nivelMax = 1;
	private static PDFPage mainPage;
	public String strDestino;

	private List estructuraBookmarks;

	private String strSQL;
	private String pymeRFC;
	private BigDecimal pymePK;
	private BigDecimal promotoriaPK;
	private BigDecimal numeroNE;

	private int resultCode = -1;
	private String MsjAIGlobal = "";
	private List ExcpPDF=new ArrayList<String>();
	
	//Reporte Problema #196851
	PDF plantilla = null;

	static {
		bookmarksOrdenados = new ArrayList();	
	}
	public DocumentosExpediente() {

		PDF.setLicenseKey("GFH70E24908BF94");
		int i=0;
		String key=null;
		while(true)
		{
			key=AccesoListPDFExcep.getParametro("ARCH_"+i);
			if(key!=null)
				ExcpPDF.add(key);
			else 
				break;
			i++;
		}
	}

	public void start() throws Throwable{

		String fechaFin="";
		String fechaInicio="";
		boolean banderaErroresMailAdm = false;
		boolean exito = false;
		AccesoDB con = new AccesoDB("ECONTRACT");
		String mailAdmin = "";
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date1 = new Date();
			fechaInicio = dateFormat.format(date1);

			boolean erroresArm=armar();
			boolean erroresInt = integracion();
			if ((erroresInt) && (erroresArm)) {
				banderaErroresMailAdm = true;
			} else 
				banderaErroresMailAdm = false;

			Date date2 = new Date();
			fechaFin = dateFormat.format(date2);

			strSQL = "CALL ECON_PKG_LIBERA_FLUJOS.AVANZA_FLUJO()";
			con.conexionDB();
			log.debug("Se ejecuta el procedimiento ->avanza_flujo()");  
			con.ejecutaUpdateDB(strSQL);	
			log.debug("Termina ejecución de procedimiento ->avanza_flujo()");
			mailAdmin=getCorreosAdmin(con);
			exito =true;

			//Cambio version 2.11.19 a 2.16 de libreria bfopdf
			plantilla.close();
			//plantilla=null;
		}
		catch (Exception e) {
			banderaErroresMailAdm = true;
			MsjAIGlobal=e.getMessage();
			e.printStackTrace();
			throw new Throwable(e.getMessage());
		}
		finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
			}
			// MandaCorreoAdmin(fechaInicio, fechaFin, banderaErroresMailAdm, 
			//            MsjAIGlobal, mailAdmin);   
		}	
	}
	private boolean integracion() {
		boolean errorGlobal = false;
		AccesoDB con = new AccesoDB();
		AccesoDB bitcon = new AccesoDB();
		AccesoDB econ = new AccesoDB("ECONTRACT");
		Registros reg;
		Registros DatanumeroNE;
		List params = new ArrayList();
		boolean exito = false,exitoEmailBand=false;
		boolean errorExpediente = false,existErrores=false;
		double tamano_Exp=0;
		String msjErrorExpediente = "";
		log.info("Inicia Integración de Expedientes");
		try {
			con.conexionDB();
			econ.conexionDB();
			bitcon.conexionDB();
			String strSQL = "select RFC_PYME,CLAVE_PROMOTORIA,USUARIO_OID_MESA,MAIL_OID_MESA,ES_CONVENIO_UNICO,max(TO_CHAR(DFECHA_CREA_EXPD,'YYYY-MM-DD HH24:MI:SS')) as DFECHA_CREA_EXPD FROM WWW_DOCUMENT where CLAVE_PROMOTORIA " + 
					"IS NOT NULL AND CS_CREA_EXPED='I' AND TP_CATALOG <>99\n" + 
					" group by RFC_PYME,CLAVE_PROMOTORIA,USUARIO_OID_MESA,MAIL_OID_MESA,ES_CONVENIO_UNICO";
			String SQLPymePk = 
					"select ECON_OPE_PYMES_PK from econ_ope_pymes where VRFC=?";
			String SQLnumeroNE = 
					"SELECT IC_NAFIN_ELECTRONICO FROM COMREL_NAFIN WHERE IC_EPO_PYME_IF = (SELECT IC_PYME FROM COMCAT_PYME WHERE CG_RFC =?) AND CG_TIPO = 'P'";

			String promotoria = "", PymePk = "", numeroNE = "", user = 
					"", esConvenioUnico = "", correo = "",fecha_prog="";
			reg = con.consultarDB(strSQL);
			String fechaActual = null;

			for (int i = 0; reg.next(); i++) {
				tamano_Exp=0;
				exito = false;
				errorExpediente = false;
				exitoEmailBand=false;
				msjErrorExpediente = "";
				existErrores=false;

				try {
					pymeRFC = reg.getString("RFC_PYME");
					promotoria = reg.getString("CLAVE_PROMOTORIA");
					user = reg.getString("USUARIO_OID_MESA");
					esConvenioUnico = getConvenioUnico(pymeRFC,con);//reg.getString("ES_CONVENIO_UNICO");
					correo = reg.getString("MAIL_OID_MESA");
					fecha_prog=reg.getString("DFECHA_CREA_EXPD");
					fechaActual = getFechaActual(con);
				} catch (Exception e) {
					errorExpediente = true;
					msjErrorExpediente = 
							"Error al obtener RFC y Clave de Promotoria";
					//Guarda en bitacora
					guardaBitacora(bitcon, "FATAL_ERROR", "I", fechaActual, "N/A", 
							msjErrorExpediente, "S","SYSDATE","0");
					continue;
				}

				params.clear();
				params.add(pymeRFC);
				try {
					Registros DataPymePk = econ.consultarDB(SQLPymePk, params);
					DataPymePk.next();
					PymePk = DataPymePk.getString("ECON_OPE_PYMES_PK");
				} catch (Exception e) {
					errorExpediente = true;
					msjErrorExpediente = "Error al obtener la clave de la PYME";
					//Guarda en bitacora
					guardaBitacora(bitcon, pymeRFC, "I", fechaActual, "N/A", 
							msjErrorExpediente, "S","SYSDATE","0");
					existErrores=true;
					MandaCorreo(pymeRFC, numeroNE, existErrores, con, user, correo);
					continue;
				}

				try {
					DatanumeroNE = con.consultarDB(SQLnumeroNE, params);
					DatanumeroNE.next();
					numeroNE = DatanumeroNE.getString("IC_NAFIN_ELECTRONICO");

				} catch (Exception e) {
					errorExpediente = true;
					msjErrorExpediente = "Error al obtener IC_NAFIN_ELECTRONICO";
					//Guarda en bitacora
					guardaBitacora(bitcon, pymeRFC, "I", fechaActual, "N/A", 
							msjErrorExpediente, "S","SYSDATE","0");
					existErrores=true;
					MandaCorreo(pymeRFC, numeroNE, existErrores, con, user, correo);
					continue;
				}
				try {
					log.info("Pyme RFC: " + pymeRFC + "\n" + "Promotoria: " + 
							promotoria + "\n" + "Pyme PK: " + PymePk + "\n" + "numero NE: " + 
							numeroNE);
					tamano_Exp=integrarExpediente(pymeRFC, promotoria, PymePk, numeroNE, esConvenioUnico, 
							con, econ,bitcon, user,fecha_prog);
					exito = true;

				} catch (Exception e) {
					e.printStackTrace();
					errorExpediente = true;
					msjErrorExpediente = e.toString();
					//Guarda en bitacora
					guardaBitacora(bitcon, pymeRFC, "I", fechaActual, "N/A", 
							msjErrorExpediente, "S",fecha_prog,"0");
					existErrores=true;

					continue;
				} finally {
					log.debug("Éxito en integración de expediente:"+exito);
					if (exito) {
						guardaBitacora(bitcon, pymeRFC, "I", fechaActual,numeroNE, 
								"Se Integró con éxito", "N",fecha_prog,new DecimalFormat("##.##").format(tamano_Exp));
					}

					if (con.hayConexionAbierta()) {
						con.terminaTransaccion(exito);
					}
					if (econ.hayConexionAbierta()) {
						econ.terminaTransaccion(exito);
					}
					try {
						MandaCorreo(pymeRFC, numeroNE, existErrores, con, user, correo);
						banderaCorreoEnviado(con,pymeRFC);
						exitoEmailBand=true;
					}
					catch (Exception e) {
						e.printStackTrace();
						exitoEmailBand=false;
					}finally {
						if (con.hayConexionAbierta()) {
							con.terminaTransaccion(exitoEmailBand);
						}
					}
				}
			}
		} catch (Exception e) {
			errorGlobal = true;
			e.printStackTrace();
			exito = false;
			MsjAIGlobal = e.getMessage();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			if (econ.hayConexionAbierta()) {
				econ.terminaTransaccion(exito);
				econ.cierraConexionDB();
			}
			if (bitcon.hayConexionAbierta()) {
				bitcon.terminaTransaccion(exito);
				bitcon.cierraConexionDB();
			}
		}
		return errorGlobal;
	}

	//Proceso de Armado  

	private boolean armar() {
		boolean errorGlobal = false;
		AccesoDB con = new AccesoDB();
		AccesoDB bitcon = new AccesoDB();
		AccesoDB econ = new AccesoDB("ECONTRACT");
		Registros reg;
		Registros DatanumeroNE;
		List params = new ArrayList();
		boolean exito = false,exitoEmailBand=false;
		boolean existErrores = false;
		boolean errorExpediente = false;
		double tamano_arch=0.0;
		String msjErrorExpediente = "";
		log.info("Inicia Armado de Expedientes");
		try {
			con.conexionDB();
			econ.conexionDB();
			bitcon.conexionDB();
			String strSQL = "select RFC_PYME,CLAVE_PROMOTORIA,USUARIO_OID_MESA,MAIL_OID_MESA,ES_CONVENIO_UNICO,max(TO_CHAR(DFECHA_CREA_EXPD, 'YYYY-MM-DD HH24:MI:SS')) as DFECHA_CREA_EXPD FROM WWW_DOCUMENT where CLAVE_PROMOTORIA " + 
					"IS NOT NULL AND CS_CREA_EXPED='A' AND TP_CATALOG <>99\n" + 
					" group by RFC_PYME,CLAVE_PROMOTORIA,USUARIO_OID_MESA,MAIL_OID_MESA,ES_CONVENIO_UNICO";

			String SQLPymePk = 
					"select ECON_OPE_PYMES_PK from econ_ope_pymes where VRFC=?";
			String SQLnumeroNE = 
					"SELECT IC_NAFIN_ELECTRONICO FROM COMREL_NAFIN WHERE IC_EPO_PYME_IF = (SELECT IC_PYME FROM COMCAT_PYME WHERE CG_RFC =?) AND CG_TIPO = 'P'";

			String promotoria = "", PymePk = "", numeroNE = "", user = 
					"", esConvenioUnico = "", correo = "",fecha_prog="";
			reg = con.consultarDB(strSQL);
			String fechaActual = null;

			for (int i = 0; reg.next(); i++) {
				exito = false;
				existErrores = false;
				errorExpediente = false;
				exitoEmailBand=false;
				msjErrorExpediente = "";

				try {
					pymeRFC = reg.getString("RFC_PYME");
					promotoria = reg.getString("CLAVE_PROMOTORIA");
					user = reg.getString("USUARIO_OID_MESA");
					esConvenioUnico = getConvenioUnico(pymeRFC,con);//reg.getString("ES_CONVENIO_UNICO");
					correo = reg.getString("MAIL_OID_MESA");
					fecha_prog=reg.getString("DFECHA_CREA_EXPD");
					fechaActual = getFechaActual(con);
					
					log.debug("pymeRFC: "+pymeRFC +" promotoria: "+promotoria+" user: "+user+" esConvenioUnico: "+esConvenioUnico);
					
				} catch (Exception e) {
					errorExpediente = true;
					msjErrorExpediente = 
							"Error al obtener RFC y Clave de Promotoria";
					//Guarda en bitacora
					guardaBitacora(bitcon, "FATAL_ERROR", "A", fechaActual, "N/A", 
							msjErrorExpediente, "S","SYSDATE","0");
					log.debug(msjErrorExpediente);
					continue;
				}

				params.clear();
				params.add(pymeRFC);
				try {
					log.debug("SQL: "+SQLPymePk + " /n params: "+params);
					Registros DataPymePk = econ.consultarDB(SQLPymePk, params);
					DataPymePk.next();
					PymePk = DataPymePk.getString("ECON_OPE_PYMES_PK");
					log.debug("PymePK: "+PymePk);
				} catch (Exception e) {
					errorExpediente = true;
					msjErrorExpediente = "Error al obtener la clave de la PYME";
					log.debug(msjErrorExpediente);
					//Guarda en bitacora
					guardaBitacora(bitcon, pymeRFC,"A", fechaActual, "N/A", 
							msjErrorExpediente, "S","SYSDATE","0");
					existErrores=true;
					MandaCorreo(pymeRFC, numeroNE, existErrores, con, user, correo);
					continue;
				}
				params.clear();
				params.add(pymeRFC);
				try {
					log.debug("SQL: "+SQLnumeroNE + " params: "+params);
					DatanumeroNE = con.consultarDB(SQLnumeroNE, params);
					DatanumeroNE.next();
					numeroNE = DatanumeroNE.getString("IC_NAFIN_ELECTRONICO");

					log.debug("numeroNE: "+numeroNE);
				} catch (Exception e) {
					errorExpediente = true;
					msjErrorExpediente = "Error al obtener IC_NAFIN_ELECTRONICO";
					log.debug(msjErrorExpediente);
					//Guarda en bitacora
					guardaBitacora(bitcon, pymeRFC, "A", fechaActual, "N/A", 
							msjErrorExpediente, "S","SYSDATE","0");
					existErrores=true;
					MandaCorreo(pymeRFC, numeroNE, existErrores, con, user, correo);
					continue;
				}
				try {
					log.info("Pyme RFC: " + pymeRFC + "\n" + "Promotoria: " + 
							promotoria + "\n" + "Pyme PK: " + PymePk + "\n" + "numero NE: " + 
							numeroNE);

					tamano_arch= integrarExpedienteNuevo(pymeRFC, promotoria, PymePk, numeroNE, 
							esConvenioUnico, con, econ,bitcon, user,fecha_prog);
					exito =true;


				} catch (Exception e) {
					e.printStackTrace();
					errorExpediente = true;
					msjErrorExpediente = e.toString();
					//Guarda en bitacora
					guardaBitacora(bitcon, pymeRFC, "A", fechaActual, "N/A", 
							msjErrorExpediente, "S","SYSDATE","0");
					existErrores=true;
					continue;
				} finally {
					log.debug("Éxito en integración de expediente:"+exito);
					if (exito) {
						guardaBitacora(bitcon, pymeRFC, "A", fechaActual, numeroNE, 
								"Se Armó con éxito", "N",fecha_prog,new DecimalFormat("#.##").format(tamano_arch));
					}

					if (con.hayConexionAbierta()) {
						con.terminaTransaccion(exito);
					}
					if (econ.hayConexionAbierta()) {
						econ.terminaTransaccion(exito);
					}
					try {
						MandaCorreo(pymeRFC, numeroNE, existErrores, con, user, correo);
						banderaCorreoEnviado(con,pymeRFC);
						exitoEmailBand=true;
					}
					catch (Exception e) {
						e.printStackTrace();
						exitoEmailBand=false;
					}finally {
						if (con.hayConexionAbierta()) {
							con.terminaTransaccion(exitoEmailBand);
						}
					}
				}
			}
		} catch (Exception e) {
			errorGlobal = true;
			e.printStackTrace();
			exito = false;
			MsjAIGlobal = e.getMessage();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			if (econ.hayConexionAbierta()) {
				econ.terminaTransaccion(exito);
				econ.cierraConexionDB();
			}
			if (bitcon.hayConexionAbierta()) {
				bitcon.terminaTransaccion(exito);
				bitcon.cierraConexionDB();
			}
		}
		return errorGlobal;
	}

	private HashMap getBookmarks(PDF bookmarkedPDF, boolean lowerCase) {
		return getBookmarksPagina(bookmarkedPDF, lowerCase, null);
	}

	private HashMap getBookmarksPagina(PDF bookmarkedPDF, boolean lowerCase, 
			HashMap mapBookmarkPagina) {
		HashMap bookmarks = new HashMap();
		iterateBookmarks(bookmarks, bookmarkedPDF.getBookmarks(), "", lowerCase, 
				mapBookmarkPagina);
		return bookmarks;
	}

	private void iterateBookmarks(HashMap container, List bookmarksList, 
			String prefix, boolean lowerCase, 
			HashMap mapBookmarkPagina) {
		PDFBookmark bookmark = null;
		String bookmarkName = null;

		int pageNumber = 0;

		Iterator itBookmarks = bookmarksList.iterator();

		while (itBookmarks.hasNext()) {
			bookmark = (PDFBookmark)itBookmarks.next();

			if (lowerCase) {
				bookmarkName = prefix + bookmark.getName().trim().toLowerCase();
				container.put(bookmark.getName().trim().toLowerCase(), 
						bookmarkName);
			} else {
				bookmarkName = prefix + bookmark.getName().trim();
				container.put(bookmark.getName().trim(), bookmarkName);
			}

			// if(mapBookmarkPagina != null && bookmark.getAction() != null) {
			if (mapBookmarkPagina != null && bookmark.getAction() != null && 
					bookmark.getAction().getPage() != null) {
				pageNumber = bookmark.getAction().getPage().getPageNumber();

				if (pageNumber > 1) {
					mapBookmarkPagina.put(bookmark.getName().trim(), 
							new Integer(pageNumber));
				}

			}


			bookmarksOrdenados.add(bookmark.getName().trim());

			iterateBookmarks(container, bookmark.getBookmarks(), bookmarkName + "/", 
					lowerCase, mapBookmarkPagina);
		}
	}

	private void setBookmarksPages(PDF bookmarkedPDF, HashMap bookmarkPages) {
		iterateForBookmarkPages(bookmarkedPDF, bookmarkPages, 
				bookmarkedPDF.getBookmarks());
	}

	private void iterateForBookmarkPages(PDF bookmarkedPDF, HashMap bookmarkPages, List bookmarksList) {
		PDFBookmark bookmark = null;
		String bookmarkName = null;
		Integer page = null;

		Iterator itBookmarks = bookmarksList.iterator();
		while(itBookmarks.hasNext()) {
			bookmark = (PDFBookmark)itBookmarks.next();

			try {
				bookmarkName = bookmark.getName().trim();
				if(bookmarkPages.containsKey(bookmarkName)) {
					page = (Integer)bookmarkPages.get(bookmarkName);
					bookmark.setAction(PDFAction.goTo( bookmarkedPDF.getPage(page.intValue()) ));
				} else {
					bookmark.setAction(PDFAction.goTo( bookmarkedPDF.getPage(0) ));
					//bookmark.setAction(null);
				}
			} catch(Exception e) {
				e.printStackTrace();
				log.debug("4) *** Page: " + page + " bookmarkName: " + bookmarkName + " bookmarkedPDF totalPages: " + bookmarkedPDF.getPages().size());
				bookmark.setAction(PDFAction.goTo( bookmarkedPDF.getPage(0) ));
			}

			//itBookmarks = null;

			iterateForBookmarkPages(bookmarkedPDF, bookmarkPages, bookmark.getBookmarks());
		}
	}

	private ByteArrayOutputStream getHojaBlanco() throws FileNotFoundException, 
	IOException {
		URL url;
		url = 
				DocumentosExpediente.class.getClassLoader().getResource("hojaBlanco.pdf");
		File filePlantilla = new File(url.getPath());
		FileInputStream fis = new FileInputStream(filePlantilla);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		IOUtils.copy(fis, bos);
		return bos;
	}

	private void validarPlantillaInfo() throws IOException {

		URL url;
		url = 
				DocumentosExpediente.class.getClassLoader().getResource("PLANTILLAEFILE.pdf");
		File filePlantilla = new File(url.getPath());

		plantilla = new PDF(new PDFReader(filePlantilla));
		estructuraBookmarks = plantilla.getBookmarks();

		if (filePlantilla.lastModified() != fechaModPlantilla) {
			listaBookmarks = new ArrayList();

			plantilla = new PDF(new PDFReader(filePlantilla));

			mainPage = plantilla.getPage(0);

			estructuraBookmarks = plantilla.getBookmarks();

			HashMap bookmarks = getBookmarks(plantilla, false);
			bookmarksPDF = (Map)bookmarks.clone();
			//Cambio de version 2.11.19 a 2.16 de libreria bfopdf
			//plantilla.close();

			String[] estructuraMarcador = null;
			String marcador = null;

			Iterator itBookmarks = bookmarks.keySet().iterator();
			while (itBookmarks.hasNext()) {
				marcador = (String)bookmarks.get(itBookmarks.next());
				estructuraMarcador = marcador.split("[/]");
				if (estructuraMarcador.length > nivelMax)
					nivelMax = estructuraMarcador.length;
				listaBookmarks.add(marcador);
			}

		}
		/* Cambio de version 2.11.19 a 2.16 de libreria bfopdf
		try {
			if (plantilla != null)
				plantilla.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		plantilla = null;
		 */
	}

	private int getNextBookmarkPage(HashMap bookmarkPage, 
			int currentBookmarkPage) {
		int result = 0, currentValue;

		int lastValue = currentBookmarkPage;

		String bookmarkName = null;

		Iterator itBookmarks = bookmarkPage.keySet().iterator();
		while (itBookmarks.hasNext()) {

			bookmarkName = (String)itBookmarks.next();
			currentValue = ((Integer)bookmarkPage.get(bookmarkName)).intValue();

			if (currentValue > currentBookmarkPage) {

				if (result == 0)
					result = currentValue;

				if (currentValue < result) {
					result = currentValue;
				}
			}
		}

		return result;
	}


	private String getBookmarkArchivo(HashMap bookmarkArchivo, String RFC, 
			String promotoria, String tipo, 
			AccesoDB con) {
		String resultMessage = null;
		List params = new ArrayList();

		Registros result = null;

		int i = 0;
		ArrayList archivos = null;

		String marcadoresNoValidos = "";
		String nombreDocumento = null;
		String marcador = null;

		// Obtener el mapeo bookmark - archivo
		strSQL = 
				"select VNOMBRE_EXP_DIG,VMARCADOR_EFILE from ECON_CAT_DOCUMENTOS where VNOMBRE_EXP_DIG in (\n" + 
						"    select SUBSTR(NAME,INSTR(NAME,'|')+1) from WWW_DOCUMENT where RFC_PYME=? and CLAVE_PROMOTORIA=? and CS_CREA_EXPED=? and TP_CATALOG=13)\n" + 
						"     group by VNOMBRE_EXP_DIG,VMARCADOR_EFILE ORDER BY VMARCADOR_EFILE";

		params.add(RFC);
		params.add(promotoria);
		params.add(tipo);
		result = con.consultarDB(strSQL, params);

		for (i = 0; result.next(); i++) {

			//row = result.getRow(i);

			nombreDocumento = result.getString("VNOMBRE_EXP_DIG");
			marcador = result.getString("VMARCADOR_EFILE");

			if (bookmarkArchivo.containsKey(marcador)) {
				archivos = (ArrayList)bookmarkArchivo.get(marcador);
			} else {
				archivos = new ArrayList();
			}

			if (!bookmarksPDF.containsKey(result.getString("VMARCADOR_EFILE"))) {
				marcadoresNoValidos += 
						", '" + result.getString("VMARCADOR_EFILE") + "'";
			}

			archivos.add(nombreDocumento);
			bookmarkArchivo.put(marcador, archivos);

		}

		if (!"".equals(marcadoresNoValidos)) {
			resultCode = 3;
			resultMessage = 
					"Error en la parametrización de los marcadores. Comunicarse con el administrador. Marcador(es) no encontrado(s): " + 
							marcadoresNoValidos.substring(1);
			resultMessage = 
					"{\"code\": " + resultCode + ", \"message\" : \"" + resultMessage + 
					"\"}";
		}

		return resultMessage;
	}

	private double integrarExpediente(String RFC, String promotoria, String PymePk, 
			String numNE, String esConvenioUnico, AccesoDB con, AccesoDB econ, AccesoDB bitcon,
			String usuarioProgExp, 
			String fecha_prog) throws java.io.IOException, 
			Exception {

		List params = new ArrayList();
		String fechaActual = null;
		String ErrorG="Error inicial";
		int fileSize=0;
		try
		{
			log.debug("integrarExpediente(E)");
			pymeRFC = RFC;
			pymePK = new BigDecimal(PymePk);
			numeroNE = new BigDecimal(numNE);
			promotoriaPK = new BigDecimal(promotoria);

			PDF expedientePDF = null;
			PDF efilePDF = null;
			PDF archivoPDF = null;
			PDFPage documentPage = null;

			ByteArrayOutputStream documentoEFile = null;

			int i = 0, j = 0, pagina = 0, totalPaginas = 0;
			Integer paginaActual = null;
			HashMap bookmarkArchivo = null;
			HashMap bookmarkPaginaPre = null;
			HashMap bookmarkPagina = null;
			HashMap paginaBookmark = null;

			ArrayList archivos = null;
			List estructuraBookmarksEFILE = null;

			ErrorG="validando Plantilla Info";

			validarPlantillaInfo();
			HashMap updateBookmar = new HashMap();
			ErrorG="Obteniendo bookmarks";
			getBookmarkArchivo(updateBookmar, pymeRFC, promotoria, "I", econ);
			//No marcar todos los documentos en estatus 99,solo los seleccionados VARIABLES
			String DocsExp = "";
			Iterator it99 = updateBookmar.keySet().iterator();
			String llave = null;
			ArrayList listMarcadores = null;
			int iteraMarcadores = 0;
			boolean bandOK = true;
			boolean hojablanca=false;

			bookmarkArchivo = new HashMap();
			ErrorG="Obteniendo segunda vez bookmarks";
			getBookmarkArchivo(bookmarkArchivo, pymeRFC, promotoria, "I", econ);

			// Obtener el expediente de E-File
			ErrorG="No se pudo obtener expediente E-File:"+numeroNE + ".pdf";
			documentoEFile = getEfileFile(numeroNE + ".pdf", con);
			ErrorG="crear instancia de pdf : "+numeroNE + ".pdf";
			efilePDF = 
					new PDF(new PDFReader(new ByteArrayInputStream(documentoEFile.toByteArray())));
			ErrorG="obtener bookmarks pdf: "+numeroNE + ".pdf";
			estructuraBookmarksEFILE = efilePDF.getBookmarks();
			// Obtener el mapeo bookmark - pagina
			bookmarkPagina = new HashMap();
			bookmarkPaginaPre = new HashMap();
			ErrorG="Obteniendo  bookmarks de pagina: "+numeroNE + ".pdf";
			getBookmarksPagina(efilePDF, false, bookmarkPagina);
			bookmarkPaginaPre = (HashMap)bookmarkPagina.clone();

			// Obtener el mapeo pagina - bookmark
			ErrorG="Obtener el mapeo pagina - bookmark:"+numeroNE + ".pdf";
			String theKey = null;
			paginaBookmark = new HashMap();
			Iterator itBookmarks = bookmarkPagina.keySet().iterator();
			while (itBookmarks.hasNext()) {
				theKey = (String)itBookmarks.next();
				paginaBookmark.put(bookmarkPagina.get(theKey), theKey);
			}

			itBookmarks = null;

			// Crear el nuevo PDF
			expedientePDF = new PDF();
			//expedientePDF.newPage(efilePDF.getPage(0));

			// Barrer paginas del PDF, cuando se llegue a una pagina que se encuentre en bookmark/archivo:
			// Obtener el archivo y meter las páginas en el nuevo
			// Obtener bookmark/pagina para obtener el siguiente bookmark
			// Sumarle la nueva cantidad de páginas a los bookmarks siguientes y hacer el set en bookmark/pagina
			for (i = 1; i <= efilePDF.getPages().size(); i++) {
				paginaActual = new Integer(i);

				if (paginaBookmark.containsKey(paginaActual)) {
					String bookmarkName = (String)paginaBookmark.get(paginaActual);

					if (bookmarkArchivo.containsKey(bookmarkName)) {
						int paginasAgregar = 
								getNextBookmarkPage(bookmarkPaginaPre, i) - i;
						if (paginasAgregar <= 0)
							paginasAgregar = 1;

						for (j = 0; j < paginasAgregar; j++) {
							expedientePDF.newPage(efilePDF.getPage(i + j - 1));
						}

						i += paginasAgregar - 1;

						paginasAgregar = 0;
						archivos = (ArrayList)bookmarkArchivo.get(bookmarkName);
						for (j = 0; j < archivos.size(); j++) {
							hojablanca=false;

							// Agregar el nuevo archivo
							fechaActual = getFechaActual(con);

							try {
								documentoEFile = 
										getEfileFile(pymeRFC + "|" + archivos.get(j), pymeRFC, promotoriaPK, con);
								if (documentoEFile == null) {
									documentoEFile = getHojaBlanco();
									hojablanca = true;

								}
								archivoPDF = 	new PDF(new PDFReader(new ByteArrayInputStream(documentoEFile.toByteArray())));

								Map p=archivoPDF.getInfo();
								log.debug("****Este pdf es Producido por: "+p.get("Producer"));
								String pro=(String)p.get("Producer");
								if(pro!= null )
								{
									if(ExcpPDF.contains(pro))
										throw new Exception("Error de producer");
								}
							}
							catch (Exception e) {
								hojablanca = true;
								documentoEFile = getHojaBlanco();
								archivoPDF =   new PDF(new PDFReader(new ByteArrayInputStream(documentoEFile.toByteArray())));
							}

							List lPages = archivoPDF.getPages();
							
							paginasAgregar = paginasAgregar + lPages.size();
							expedientePDF.getPages().addAll(lPages);
							
							/*Iterator itPaginasNew = archivoPDF.getPages().iterator();
						while (itPaginasNew.hasNext()) {
							documentPage = (PDFPage)itPaginasNew.next();
							expedientePDF.newPage(documentPage);
							paginasAgregar++;
						}

						itPaginasNew = null;
							 */
							archivoPDF.close();
							archivoPDF = null;
							if(!hojablanca)
								guardaBitacora(bitcon, pymeRFC, "I", fechaActual, archivos.get(j) + "", 
										"Procesado Exitosamente", "N",fecha_prog,documentoEFile.toByteArray().length+"");
							else
								guardaBitacora(bitcon, pymeRFC, "I", fechaActual, archivos.get(j) + "", 
										"Error documento RFC:" + pymeRFC+", se usó hoja blanco",
										"S",fecha_prog,documentoEFile.toByteArray().length+"");
						}

						// Mover de pagina los bookmarks siguientes
						String theBookmarkName = null;
						//boolean nextBookmarkSet = false;

						int paginaAux = 0;
						int paginaBase = 
								((Integer)bookmarkPagina.get(bookmarkName)).intValue();

						Iterator paginasBookmark = bookmarkPagina.keySet().iterator();
						while (paginasBookmark.hasNext()) {
							theBookmarkName = (String)paginasBookmark.next();
							paginaAux =((Integer)bookmarkPagina.get(theBookmarkName)).intValue();

							if (theBookmarkName.equals(bookmarkName)) {
								//bookmarkPagina.put(theBookmarkName, new Integer(((Integer)bookmarkPagina.get(theBookmarkName)).intValue() - paginasAgregar));
							} else {
								if (paginaAux > 1 && paginaAux > paginaBase) {
									bookmarkPagina.put(theBookmarkName, 
											new Integer(paginaAux + paginasAgregar));
								} else if (paginaAux == 1) {
									bookmarkPagina.put(theBookmarkName, new Integer(0));
								}
							}
						}

						paginasBookmark = null;

						bookmarkArchivo.remove(bookmarkName);
						continue;
					}

				}

				if (i > 0) {
					expedientePDF.newPage(efilePDF.getPage(i - 1));
				} else {
					expedientePDF.newPage(efilePDF.getPage(i));
				}
			}
			// *****FIN**** Barrer paginas del PDF, cuando se llegue a una pagina que se encuentre en bookmark/archivo:

			efilePDF.close();
			efilePDF = null;

			pagina = expedientePDF.getPages().size();
			Iterator bookmarksRestantes = bookmarkArchivo.keySet().iterator();
			while (bookmarksRestantes.hasNext()) {
				String bookmarkName = (String)bookmarksRestantes.next();

				bookmarkPagina.put(bookmarkName, new Integer(pagina + 1));

				archivos = (ArrayList)bookmarkArchivo.get(bookmarkName);

				for (i = 0; i < archivos.size(); i++) {
					hojablanca=false;



					try {
						documentoEFile = 
								getEfileFile(pymeRFC + "|" + archivos.get(i), pymeRFC, promotoriaPK, 
										con);

						if (documentoEFile == null) {

							documentoEFile = getHojaBlanco();   
							hojablanca=true;
						}
						archivoPDF = new PDF(new PDFReader(new ByteArrayInputStream(documentoEFile.toByteArray())));
						Map p=archivoPDF.getInfo();
						log.debug("***Este pdf es Producido por: "+p.get("Producer"));
						String pro=(String)p.get("Producer");
						if(pro!= null )
						{
							if(ExcpPDF.contains(pro))
								throw new Exception("Error de producer");
						}
					}
					catch (Exception e) {

						hojablanca=true;
						documentoEFile = getHojaBlanco();
						archivoPDF = new PDF(new PDFReader(new ByteArrayInputStream(documentoEFile.toByteArray())));

					}
					ErrorG="integrando archivo..."+ archivos.get(i);
					List lPages = archivoPDF.getPages();
					pagina = pagina + lPages.size();
					
					expedientePDF.getPages().addAll(lPages);
					

					/*Iterator itPaginasNew = archivoPDF.getPages().iterator();
				while (itPaginasNew.hasNext()) {
					documentPage = (PDFPage)itPaginasNew.next();
					expedientePDF.newPage(documentPage);
					pagina++;
				}
				itPaginasNew = null;
				documentPage = null;*/


					archivoPDF.close();
					archivoPDF = null;

					documentPage = null;

					if(!hojablanca)
						guardaBitacora(bitcon, pymeRFC, "I", fechaActual, archivos.get(i) + "", 
								"Procesado Exitosamente", "N",
								fecha_prog,documentoEFile.toByteArray().length+"");
					else
						guardaBitacora(bitcon, pymeRFC, "I", fechaActual, archivos.get(i) + "", 
								"Error documento RFC:" + pymeRFC+", se usó hoja blanco", "S",
								fecha_prog,documentoEFile.toByteArray().length+"");

				}

				archivos.clear();
				archivos = null;
			}

			bookmarksRestantes = null;

			String tmpName = null;
			Integer tmpPagina = null;
			Iterator tmpBookmarks = bookmarkPagina.keySet().iterator();
			while (tmpBookmarks.hasNext()) {
				tmpName = (String)tmpBookmarks.next();
				tmpPagina = (Integer)bookmarkPagina.get(tmpName);

				if (tmpPagina.intValue() >= expedientePDF.getPages().size()) {
					tmpPagina = new Integer(expedientePDF.getPages().size() - 1);
				} else if (tmpPagina.intValue() > 1) {
					tmpPagina = new Integer(tmpPagina.intValue() - 1);
				}

				bookmarkPagina.put(tmpName, tmpPagina);
			}

			tmpBookmarks = null;
			ErrorG="Insertar los bookmarks y mapear las paginas con bookmark página";
			// Insertar los bookmarks y mapear las paginas con bookmark página
			// Guardar el archivo
			expedientePDF.getBookmarks().addAll(estructuraBookmarksEFILE);
			setBookmarksPages(expedientePDF, bookmarkPagina);

			// TODO: Validar el numero de paginas
			totalPaginas = expedientePDF.getPages().size();
			log.debug("*** Paginas PDF Final: " + 
					expedientePDF.getPages().size());

			// Pasar el PDF a bytes para su insercion
			ErrorG="Pasar el PDF a bytes para su insercion";
			documentoEFile = new ByteArrayOutputStream();
			expedientePDF.render(documentoEFile);

			expedientePDF.close();
			expedientePDF = null;

			byte[] fileBytes = documentoEFile.toByteArray();
			fileSize = fileBytes.length;
			log.debug("Tamaño del archivo final "+fileSize);
			documentoEFile.close();
			documentoEFile = null;


			strSQL = 
					"UPDATE WWW_DOCUMENT SET TP_CATALOG = 99 WHERE RFC_PYME = ? AND UPPER(TRIM(NAME)) = ?";
			log.debug("integrarExpediente(strSQL)" + strSQL);

			params.clear();
			params.add(pymeRFC);
			params.add(numeroNE + ".PDF");
			con.ejecutaUpdateDB(strSQL, params);

			// Salvar el PDF en E-File
			strSQL = 
					"INSERT INTO WWW_DOCUMENT ( " + "  NAME, MIME_TYPE, DOC_SIZE, " + "  LAST_UPDATED, BLOB_CONTENT, USUARIO, FECHA, " + 
							"  RFC_PYME, DOC_CONS, TP_CATALOG, STATUS, " + 
							"  ES_CONVENIO_UNICO, CLAVE_PROMOTORIA,DFECHA_CREA_EXPD) " + "VALUES( " + 
							"  ?, 'application/pdf', ?," + "SYSDATE, empty_blob(), ?, ?, " + 
							"  ?, 1, 2, 'A', ?, NULL,to_date(?,'yyyy-mm-dd HH24:MI:SS'))";

			String SQLFecha = 
					"SELECT FECHA FROM WWW_DOCUMENT WHERE RFC_PYME=? AND TP_CATALOG = 99 AND UPPER(TRIM(NAME)) = ?";
			log.debug("integrarExpediente(strSQL)" + SQLFecha);
			params.clear();
			params.add(pymeRFC);
			params.add(numeroNE + ".PDF");
			//String fechacarga = con.consultarDB(SQLFecha, new Object[] { pymeRFC, numeroNE + ".PDF" }).getString("FECHA");       
			Registros DataFechaCarga = con.consultarDB(SQLFecha, params);
			DataFechaCarga.next();
			String fechacarga = DataFechaCarga.getString("FECHA");
			log.debug("integrarExpediente(strSQL)" + strSQL);

			params.clear();
			params.add(numeroNE + ".pdf");
			params.add(new BigDecimal(fileSize));
			params.add(usuarioProgExp);
			params.add(fechacarga);
			params.add(pymeRFC);
			params.add(esConvenioUnico);
			params.add(fecha_prog.length()==8?(fecha_prog+" 00:00:00"):fecha_prog);
			con.ejecutaUpdateDB(strSQL, params);



			strSQL = 
					" UPDATE WWW_DOCUMENT SET " + " BLOB_CONTENT=? " + " WHERE NAME=? AND RFC_PYME=? AND TP_CATALOG=2 AND CLAVE_PROMOTORIA IS NULL";

			log.debug("integrarExpediente(strSQL)" + strSQL);
			params.clear();
			params.add(fileBytes);
			params.add(numeroNE + ".pdf");
			params.add(pymeRFC);
			con.ejecutaUpdateDB(strSQL, params);
			fileBytes = null;
			log.debug("fin inserta archivo "+strSQL);

			///////////////////DOC INFO PYME INSERT////////////////////////////////////////

			strSQL = 
					"SELECT VRAZON_SOCIAL FROM ECON_OPE_PYMES WHERE ECON_OPE_PYMES_PK = ?";
			log.debug("integrarExpediente(strSQL)" + strSQL);
			params.clear();
			params.add(pymePK);
			Registros DataNombrePyme = econ.consultarDB(strSQL, params);
			DataNombrePyme.next();
			String nombrePyme = DataNombrePyme.getString("VRAZON_SOCIAL");

			strSQL = "DELETE FROM WWW_DOC_INFO WHERE NAME = ? AND RFC_PYME = ? ";
			log.debug("integrarExpediente(strSQL)" + strSQL);

			params.clear();
			params.add(numeroNE + ".pdf");
			params.add(pymeRFC);
			con.ejecutaUpdateDB(strSQL, params);
			strSQL = "INSERT INTO WWW_DOC_INFO VALUES ( ?, ?, ?, 1)";
			log.debug("numeroNE: " + numeroNE + " pymeRFC: " + pymeRFC + 
					" nombrePyme: " + nombrePyme);

			params.add(nombrePyme);
			con.ejecutaUpdateDB(strSQL, params);

			////////////////////FIN   DOC INFO PYME INSERT////////////////////////////////////////
			strSQL = "CALL PROC_REGISTRA_CARGA ( ?, 2, ?)";
			log.debug("integrarExpediente(strSQL)" + strSQL);
			params.clear();
			params.add(numeroNE);
			params.add(usuarioProgExp);
			con.ejecutaUpdateDB(strSQL, params);
			//No marcar todos los documentos en estatus 99,solo los seleccionados

			strSQL = 
					"UPDATE WWW_DOCUMENT SET TP_CATALOG = 99 WHERE RFC_PYME = ? AND NAME =? AND CLAVE_PROMOTORIA = ?";
			log.debug("integrarExpediente(strSQL)" + strSQL);


			bandOK = true;
			while (it99.hasNext() && bandOK) {
				llave = (String)it99.next();
				listMarcadores = (ArrayList)updateBookmar.get(llave);
				for (iteraMarcadores = 0; iteraMarcadores < listMarcadores.size() && bandOK; 
						iteraMarcadores++) {
					DocsExp = 
							pymeRFC + "|" + listMarcadores.get(iteraMarcadores).toString();
					params.clear();
					params.add(pymeRFC);
					params.add(DocsExp);
					params.add(promotoriaPK);
					con.ejecutaUpdateDB(strSQL, params);

				}
			}
			log.debug("Finalizó expediente:"+pymeRFC);
		} catch (Exception e) {
			e.printStackTrace();
			guardaBitacora(bitcon, pymeRFC, "A", fechaActual, "ERROR GENERAL", 
					ErrorG, "S",fecha_prog,"0");
			throw new Exception("Error en proceso de integración "+e.getMessage());
		}
		return fileSize;
	}


	//Esta funcion tiene como objtivo el Armado de cada uno de los expedientes.
	private double integrarExpedienteNuevo(String RFC, String promotoria, 
			String PymePk, String numNE, String esConvenioUnico, 
			AccesoDB con, 
			AccesoDB econ, 
			AccesoDB bitcon,
			String usuarioProgExp, 
			String fecha_prog) throws java.io.IOException,Exception  {
		List params = new ArrayList();
		String fechaActual = null;
		String ErrorG="Error inicial";
		int fileSize=0;
		try {
			log.debug("integrarExpedienteNuevo(E)");
			ErrorG="obteniendo parametros";
			pymeRFC = RFC;
			pymePK = new BigDecimal(PymePk);
			numeroNE = new BigDecimal(numNE);
			promotoriaPK = new BigDecimal(promotoria);

			PDF expedientePDF = null;
			PDF documentoPDF = null;

			ByteArrayOutputStream documentoEFile = null;

			String marcador = null;
			HashMap updateBookmar = new HashMap();
			HashMap marcadores = new HashMap();
			HashMap marcadorPagina = new HashMap();
			HashMap bookmarkArchivo = null;
			ArrayList archivos = null;

			int i = 0, j = 0, pagina = 1;

			ErrorG="validando Plantilla Info";
			log.debug(ErrorG);
			validarPlantillaInfo();
			ErrorG="Obteniendo bookmarks";
			log.debug(ErrorG);
			getBookmarkArchivo(updateBookmar, pymeRFC, promotoria, "A", econ);

			log.debug("Se trajo los bookmarks");
			//No marcar todos los documentos del expediente en estatus 99
			String DocsExp = "";
			ErrorG="iterador de bookmarks";
			Iterator it99 = updateBookmar.keySet().iterator();
			String llave = null;
			ArrayList listMarcadores = null;
			int iteraMarcadores = 0;
			boolean bandOK = true;	
			boolean hojablanca=false;

			bookmarkArchivo = new HashMap();
			ErrorG="Obteniendo segunda vez bookmarks";
			getBookmarkArchivo(marcadores, pymeRFC, promotoria, "A", econ);

			// Crear el nuevo expediente
			expedientePDF = new PDF();
			ErrorG="agrega portada a documento";
			expedientePDF.newPage(mainPage);
			log.debug(" 0) *** Total bookmarksOrdenados: " + 
					bookmarksOrdenados.size());
			ErrorG="comenzando barrido para armado";
			PDFReader myREader=null;
			for (i = 0; i < bookmarksOrdenados.size(); i++) {

				ErrorG="obtener bookmark de documento";
				marcador = (String)bookmarksOrdenados.get(i);
				ErrorG="obtener bookmark de archivo "+marcador;
				if (marcador.indexOf("/") != -1) {
					marcador = marcador.substring(marcador.lastIndexOf("/") + 1);
				}
				ErrorG="obtener nombre de archivos de bookmark"+marcador;
				archivos = (ArrayList)marcadores.get(marcador);

				if (archivos != null) {
					log.debug(i+") *** Marcador: " + marcador + " archivos: " + 
							archivos + " size: " + archivos.size());
					marcadorPagina.put(marcador, new Integer(pagina));

					for (j = 0; j < archivos.size(); j++) {
						hojablanca=false;		

						fechaActual = getFechaActual(con);

						try {
							documentoEFile =  getEfileFile(pymeRFC + "|" + archivos.get(j), pymeRFC, promotoriaPK, con);

							if (documentoEFile == null) {
								documentoEFile = getHojaBlanco();
								hojablanca = true;
							} else {
								// Add try-catch cause throws Parse error at
								// byte 485921: Illegal Hex Digit u 13-May-2015
								try {
									myREader =   new PDFReader();
									myREader.setSource(new ByteArrayInputStream(documentoEFile.toByteArray()));
									myREader.setLinearizedLoader(true);
									myREader.load();
									documentoPDF = new PDF(myREader);
								} catch (IOException ioBadByteArrayExc) {
									log.debug("Entra a IOException"+ ioBadByteArrayExc.getLocalizedMessage());
									documentoEFile = getHojaBlanco();
									log.debug("-*-*-*-*- Usando Hoja en Blanco");
									documentoPDF =	new PDF(new PDFReader(new ByteArrayInputStream(documentoEFile.toByteArray())));
									hojablanca = true;
								} catch (Exception genericExc) {
									log.debug("Generic Exception trying to read file "+ genericExc.getMessage());
									documentoEFile = getHojaBlanco();
									log.debug("-*-*-*-*- Usando Hoja en Blanco Exception General");
									documentoPDF = 
											new PDF(new PDFReader(new ByteArrayInputStream(documentoEFile.toByteArray())));
									hojablanca = true;
								}
							}
							//myREader.getProgress();
							
							log.debug(i+") *** PDF: " + archivos.get(j) + " Paginas: " + 
									documentoPDF.getPages().size());

							Map p=documentoPDF.getInfo();
							log.debug("**Este pdf fue Producido por: "+p.get("Producer"));
							String pro=(String)p.get("Producer");
							if(pro!= null ){
								if(ExcpPDF.contains(pro))
									throw new Exception("Error de producer");
							}

						}catch (Exception e) {
							log.error("-*-*-*-*-"+e.getMessage());
							hojablanca = true;
							documentoEFile = getHojaBlanco();
							documentoPDF = 
									new PDF(new PDFReader(new ByteArrayInputStream(documentoEFile.toByteArray())));
							log.debug(i+") *** PDF: " + archivos.get(j) + " Paginas: " + 
									documentoPDF.getPages().size());
						}

						ErrorG="integrando archivo..."+ archivos.get(j);
						List lPages = documentoPDF.getPages();
						pagina = pagina + lPages.size();

						log.debug("doc simple: "+documentoPDF.getPages().size());
						
						expedientePDF.getPages().addAll(lPages);
						
						log.debug("doc total: "+expedientePDF.getPages().size());
						
						documentoPDF.close();
						documentoPDF = null;
						if(!hojablanca)
							guardaBitacora(bitcon, pymeRFC, "A", fechaActual, archivos.get(j) + "", 
									"Procesado Exitosamente", "N",
									fecha_prog,documentoEFile.toByteArray().length+"");
						else
							guardaBitacora(bitcon, pymeRFC, "A", fechaActual, archivos.get(j) + "", 
									"Error al obtener documento RFC:" + pymeRFC+", se usó hoja en blanco",
									"S",fecha_prog,documentoEFile.toByteArray().length+"");


					}
				}

				marcadores.remove(marcador);
			}
			ErrorG="En este punto, se encuentra creado el nuevo PDF en el orden de los bookmarks";
			// En este punto, se encuentra creado el nuevo PDF en el orden de los bookmarks
			log.debug(" *** Total Bookmarks: " + estructuraBookmarks.size() + 
					" Clase: " + 
					estructuraBookmarks.getClass().getName());
			expedientePDF.getBookmarks().addAll(estructuraBookmarks);
			setBookmarksPages(expedientePDF, marcadorPagina);

			// TODO: Validar el no de paginas
			ErrorG="numero de paginas exp armado "+expedientePDF.getPages().size();
			log.debug(" 3) Paginas PDF Final: " + 
					expedientePDF.getPages().size());

			// Pasar el PDF a bytes para su insercion
			ErrorG="Pasar el PDF a bytes para su insercion";
			documentoEFile = new ByteArrayOutputStream();
			//Add try catch si el documento integardo tira IOException
			try {
				expedientePDF.render(documentoEFile);
			} catch (IOException e) {
				log.error("*-*-*-* falla en el expediente Integrado"+e.getMessage());
				guardaBitacora(bitcon, pymeRFC, "A", fechaActual, "ERROR GENERAL", 	ErrorG, "S",fecha_prog,"0");
			}
			

			expedientePDF.close();
			expedientePDF = null;

			byte[] fileBytes = documentoEFile.toByteArray();
			fileSize = fileBytes.length;

			documentoEFile.close();
			documentoEFile = null;
			// Obtener la info de convenio unico * de momento está clavado pero vendra en la tabla de www_document
			//GUARDAR EN BITACORA
			strSQL = "CALL PROC_REGISTRA_CARGA ( ?, 2, ?)";
			log.debug("integrarExpedienteNuevo(strSQL)" + strSQL);
			params.clear();
			params.add(numeroNE);
			params.add("P BATCH");
			con.ejecutaUpdateDB(strSQL, params);

			// Salvar el PDF en E-File
			strSQL = 
					"INSERT INTO WWW_DOCUMENT ( " + "  NAME, MIME_TYPE, DOC_SIZE, " + "  BLOB_CONTENT, USUARIO, FECHA, " + 
							"  RFC_PYME, DOC_CONS, TP_CATALOG, STATUS, " + 
							"  ES_CONVENIO_UNICO, CLAVE_PROMOTORIA,DFECHA_CREA_EXPD) " + "VALUES (" + 
							"  ?, 'application/pdf', ?, " + 
							"   empty_blob(),?,  TO_CHAR(SYSDATE, 'dd/MM/yyyy HH24:MI:SS'), " + 
							"  ?, 1, 2, 'A', ?, NULL,to_date(?,'yyyy-mm-dd HH24:MI:SS'))";
			log.debug("insertar en wwwDocument" + strSQL);
			params.clear();
			params.add(numeroNE + ".pdf");
			params.add(new BigDecimal(fileSize));
			params.add(usuarioProgExp);
			params.add(pymeRFC);
			params.add(esConvenioUnico);
			params.add(fecha_prog);
			con.ejecutaUpdateDB(strSQL, params);

			strSQL = 
					" UPDATE WWW_DOCUMENT SET " + " BLOB_CONTENT=? " +
							" WHERE NAME=? AND RFC_PYME=? AND TP_CATALOG=2 AND CLAVE_PROMOTORIA IS NULL";
			log.debug("update en wwwDocument" + strSQL);
			params.clear();
			params.add(fileBytes);
			params.add(numeroNE + ".pdf");
			params.add(pymeRFC);
			con.ejecutaUpdateDB(strSQL, params);
			fileBytes = null;

			//// Insertar en tabla WWW_DOC_INFO			   
			strSQL = 
					"SELECT VRAZON_SOCIAL FROM ECON_OPE_PYMES WHERE ECON_OPE_PYMES_PK = ?";
			log.debug("integrarExpedienteNuevo(strSQL)" + strSQL);
			params.clear();
			params.add(pymePK.toString());
			Registros DataNombrePyme = econ.consultarDB(strSQL, params);
			DataNombrePyme.next();
			String nombrePyme = DataNombrePyme.getString("VRAZON_SOCIAL");
			params.clear();

			params.add(numeroNE + ".pdf");
			params.add(pymeRFC);
			strSQL = "INSERT INTO WWW_DOC_INFO VALUES ( ?, ?, ?, 1)";
			log.debug("integrarExpedienteNuevo(strSQL)" + strSQL);
			log.debug("numeroNE: " + numeroNE + " pymeRFC: " + pymeRFC + 
					" nombrePyme: " + nombrePyme);
			params.add(nombrePyme);
			con.ejecutaUpdateDB(strSQL, params);

			// Borrar docs de E-File
			//No marcar todos los documentos en estatus 99,solo los seleccionados

			strSQL = 
					"UPDATE WWW_DOCUMENT SET TP_CATALOG = 99 WHERE RFC_PYME = ? AND NAME =? AND CLAVE_PROMOTORIA = ?";
			log.debug("integrarExpedienteNuevo(strSQL)" + strSQL);
			while (it99.hasNext() && bandOK) {
				llave = (String)it99.next();
				listMarcadores = (ArrayList)updateBookmar.get(llave);
				for (iteraMarcadores = 0; 
						iteraMarcadores < listMarcadores.size() && bandOK; 
						iteraMarcadores++) {
					DocsExp = 
							pymeRFC + "|" + listMarcadores.get(iteraMarcadores).toString();
					params.clear();
					params.add(pymeRFC);
					params.add(DocsExp);
					params.add(promotoriaPK);
					con.ejecutaUpdateDB(strSQL, params);
				}
			}

			if (marcadores != null)
				marcadores.clear();
			marcadores = null;

			if (marcadorPagina != null)
				marcadorPagina.clear();
			marcadorPagina = null;

			if (bookmarkArchivo != null)
				bookmarkArchivo.clear();
			bookmarkArchivo = null;

			if (archivos != null)
				archivos.clear();
			archivos = null;
			log.debug("integrarExpedienteNuevo(S)");
		} catch (Exception e) {
			e.printStackTrace();
			guardaBitacora(bitcon, pymeRFC, "A", fechaActual, "ERROR GENERAL", 
					ErrorG, "S",fecha_prog,"0");

			throw new Exception("Error en proceso de armado "+e.getMessage());
		}
		return fileSize;
	}

	private ByteArrayOutputStream getEfileFile(String nombreArchivo, 
			AccesoDB con) throws Exception {
		log.debug("getEfileFile(E)");

		List params = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte[] arrByte = new byte[4096];
		ByteArrayOutputStream documentoEFile = null;
		InputStream inStream = null;
		// Traerlo de E-File a EContract
		strSQL = 
				"SELECT BLOB_CONTENT FROM WWW_DOCUMENT WHERE NAME in(?,?) AND RFC_PYME IS NOT NULL AND ROWNUM = 1";

		ps = con.queryPrecompilado(strSQL.toString());
		ps.setString(1, nombreArchivo);
		ps.setString(2, nombreArchivo.toUpperCase());
		rs = ps.executeQuery();

		//documentoEFile
		if(rs.next())
			inStream = rs.getBinaryStream("BLOB_CONTENT");
		rs.close();
		ps.close();
		if (inStream != null) {

			documentoEFile = new ByteArrayOutputStream();

			int i = 0;
			while ((i = inStream.read(arrByte, 0, arrByte.length)) != -1) {
				documentoEFile.write(arrByte, 0, i);
			}

			inStream.close();
		}

		log.debug("getEfileFile(S)");
		return documentoEFile;

	}

	private ByteArrayOutputStream getEfileFile(String nombreArchivo, 
			String thePymeRFC, BigDecimal thePromotoriaPK, 
			AccesoDB con) throws SQLException, 
			IOException {
		log.debug("getEfileFile(String,String,BigDecimal,AccesoDB)(E)");
		//GenericDMLImpl genericDML = new GenericDMLImpl();
		List params = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte[] arrByte = new byte[4096];
		ByteArrayOutputStream documentoEFile = null;
		InputStream inStream = null;

		// Traerlo de E-File a EContract
		strSQL = 
				"SELECT BLOB_CONTENT, 'PDF', DOC_SIZE FROM WWW_DOCUMENT WHERE NAME = ? AND RFC_PYME = ? AND CLAVE_PROMOTORIA = ? AND ROWNUM = 1";
		ps = con.queryPrecompilado(strSQL.toString());
		//ps.setString(1,pymePK.toString());
		ps.setString(1, nombreArchivo);
		ps.setString(2, thePymeRFC);
		ps.setString(3, thePromotoriaPK.toString());
		rs = ps.executeQuery();
		rs.next();
		//documentoEFile
		inStream = rs.getBinaryStream("BLOB_CONTENT");

		rs.close();
		ps.close();
		if (inStream != null) {

			documentoEFile = new ByteArrayOutputStream();

			int i = 0;
			while ((i = inStream.read(arrByte, 0, arrByte.length)) != -1) {
				documentoEFile.write(arrByte, 0, i);
			}

			inStream.close();
		}
		log.debug("getEfileFile(String,String,BigDecimal,AccesoDB)(S))");

		//System.out.println("Tamaño en la base: "+tamArch+"---Tamaño al abrir:"+documentoEFile.toByteArray().length);
		return documentoEFile;
	}
	/*
	 * Metodo para guardar el estatus de procesamiento de documento en la bitacora
	 */

	private void guardaBitacora(AccesoDB con, String RFCPyme, String tipoProces, 
			String fechaInicio, String IdDoc, String desError, 
			String banderaErrExi,String fecha_crea_exp,String doc_size) throws Exception {
		List params = new ArrayList();
		fecha_crea_exp=fecha_crea_exp.equals("")?"SYSDATE":fecha_crea_exp;
		String comodinFecha = "",sql_fecha_crea_exp="";
		if (fechaInicio == null)
			comodinFecha = ",SYSDATE";
		else
			comodinFecha = ",to_date(?,'Dy DD-Mon-YYYY HH24:MI:SS')";

		if(fecha_crea_exp.equals("SYSDATE"))
		{
			sql_fecha_crea_exp="SYSDATE";	
		}
		else
			sql_fecha_crea_exp="to_date(?,'yyyy-mm-dd HH24:MI:SS')";
		//comodinFecha="";
		strSQL = 
				"insert into WWW_BIT_ARMA_EXP (ID_BIT_ARMA_EXP_PK," +
						"RFC_PYME," +
						"CS_CREA_EXPED," +
						"DINICIO_DOCTO," +
						"DFIN_DOCTO," +
						"DOC_PROCESADO," +
						"DESCRIPCION," +
						"ID_EXP," +
						"DFECHA_CREA_EXPED," +
						"SIZE_DOC)  " + 
						"  values (BIT_ARMA_EXP_SEQ.NextVal,?,?" + comodinFecha + 
						",sysdate,?,?,?,"+sql_fecha_crea_exp+",?) ";

		params.add(RFCPyme);
		params.add(tipoProces);
		if (fechaInicio != null)
			params.add(fechaInicio);
		params.add(IdDoc);
		params.add(desError.length()>200?desError.substring(0,199):desError);
		params.add(banderaErrExi);
		if(!fecha_crea_exp.equals("SYSDATE"))	   
			params.add(fecha_crea_exp);
		params.add(doc_size);


		con.ejecutaUpdateDB(strSQL, params);
		con.terminaTransaccion(true);

	}

	private String getFechaActual(AccesoDB con) {
		String fecha = 
				"select to_char(sysdate, 'Dy DD-Mon-YYYY HH24:MI:SS') as fecha_actual " + 
						"from dual ";

		Registros dataFecha = con.consultarDB(fecha);
		dataFecha.next();
		String nombrePyme = dataFecha.getString("fecha_actual");

		return nombrePyme;

	}

	public void setPlantillaPath(String plantillaPath) {
		this.plantillaPath = plantillaPath;
	}


	public String getPlantillaPath() {
		return plantillaPath;
	}


	public void setFechaModPlantilla(long fechaModPlantilla) {
		this.fechaModPlantilla = fechaModPlantilla;
	}


	public long getFechaModPlantilla() {
		return fechaModPlantilla;
	}


	public void setListaBookmarks(ArrayList listaBookmarks) {
		this.listaBookmarks = listaBookmarks;
	}


	public ArrayList getListaBookmarks() {
		return listaBookmarks;
	}


	public void setEstructuraBookmarks(List estructuraBookmarks) {
		this.estructuraBookmarks = estructuraBookmarks;
	}


	public List getEstructuraBookmarks() {
		return estructuraBookmarks;
	}


	public void setBookmarksPDF(Map bookmarksPDF) {
		this.bookmarksPDF = bookmarksPDF;
	}


	public Map getBookmarksPDF() {
		return bookmarksPDF;
	}


	public void setNivelMax(int nivelMax) {
		this.nivelMax = nivelMax;
	}


	public int getNivelMax() {
		return nivelMax;
	}


	public void setMainPage(PDFPage mainPage) {
		this.mainPage = mainPage;
	}


	public static PDFPage getMainPage() {
		return mainPage;
	}
	private void banderaCorreoEnviado(AccesoDB con, String RFC) throws Exception {
		String strSQL="update  WWW_BIT_ARMA_EXP set CS_ENV_MAIL_EXP='S'\n" + 
				" where RFC_PYME='"+RFC+"'";
		con.ejecutaUpdateDB(strSQL);
	}
	private void MandaCorreoAdmin(String fechaInicio, String fechaFin, 
			boolean existErrores, String Msj, 
			String mail) {

		Correo correo = new Correo();
		correo.setNoResponseSender();
		if (existErrores) {
			log.debug("Se va a enviar mail: Falla Ejecución Proceso Batch!- Proceso Automatico ARMA_EXPEDIENTE");
			/*correo.enviaCorreo(mail, 
					"Falla Ejecución Proceso Batch!- Proceso Automatico ARMA_EXPEDIENTE", 
					"<br><br>" +
							"<font style='font-size:9.0pt;font-family:Arial,sans-serif'><b>" +
							"Inicio del proceso:" + fechaInicio + "<br>" + 
							"Termino del proceso:" + fechaFin + "<br><br><br>" + 
							"<span style='color:red'>El proceso Automático ARMA_EXPEDIENTE se ejecutó con errores.</span><br><br>" + 
							"Causa: " + Msj + 
							"<br><br><br>Nota:Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar " + 
							"correos electrónicos de entrada.Por favorno respondas a este mensaje" +
					"</b></font>");*/
		} else {
			log.debug("Se va a mandar mail: Ejecución Exitosa Proceso Batch!- Proceso Automatico ARMA_EXPEDIENTE");
			/*correo.enviaCorreo(mail, 
					"Ejecución Exitosa Proceso Batch!- Proceso Automatico ARMA_EXPEDIENTE", 
					"<br><br>" +
							"<font style='font-size:9.0pt;font-family:Arial,sans-serif'><b>" +
							"Inicio del proceso:" + fechaInicio + "<br>" + 
							"Termino del proceso:" + fechaFin + "<br><br><br>" + 
							"<span style='color:green'>El proceso Automático ARMA_EXPEDIENTE se ejecutó exitosamente.</span><br><br>" + 
							"<br><br>Nota: Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar " + 
							"correos electrónicos de entrada. Por favor no respondas a este mensaje" +
					"</b></font>");*/
		}

	}

	private void MandaCorreo(String pymeRFC, String numeroNE, 
			boolean existErrores, AccesoDB con, String user, 
			String mail) throws IOException,Exception {
		boolean exito=false;
		try {
			String Sql = 
					"select * from WWW_BIT_ARMA_EXP where  RFC_PYME=? and CS_ENV_MAIL_EXP='N' order by  DFIN_DOCTO asc";
			List params = new ArrayList();
			params.add(pymeRFC);
			Registros regTablaErrores = null, regTam = null;
			String cuerpoCorreo ="<html><body>"+ 
					"<font style='font-size:9.0pt;font-family:Arial,sans-serif'>" +
					"Estimado Usuario:<br><br>A continuación le informamos el resultado del procesamiento " + 
					"del expediente digital por el Proceso ARMA_EXPEDIENTE</font><br><br><br>";
			cuerpoCorreo += 
					"<table width=1230 border=1 cellspacing=0 cellpadding=2 bordercolor=000000" +
							"	style='font-size:9.0pt;font-family:Arial,sans-serif'>" +
							"                 <tr>" +
							"                    <td width=150>RFC</td>" + 
							"                   	 <td width=150>Documento</td>" + 
							"                    <td width=180>Fecha en que se programó el expediente</td>" + 
							"                    <td width=150>Fecha Inicio Procesamiento del expediente</td>" + 
							"                    <td width=150>Fecha fin Procesamiento del Expediente</td>" + 
							"                    <td width=100>Tamaño</td>" + 
							"                    <td width=150>Usuario</td>" + 
							"                    <td width=200>Resultado</td>" + 
							"                 </tr>";	
			//si no hubo errores

			regTablaErrores = con.consultarDB(Sql, params);
			if (regTablaErrores.getNumeroRegistros() > 0) {

				String tamDoc ="";
				double iTamDoc=0;				  
				String color="";
				while (regTablaErrores.next()) {
					tamDoc=regTablaErrores.getString("SIZE_DOC");
					tamDoc=tamDoc==null || tamDoc.equals("")?"0":tamDoc;
					iTamDoc=Double.parseDouble(tamDoc);
					iTamDoc=iTamDoc/1024/1024;						
					color=regTablaErrores.getString("ID_EXP").equals("S")?"#FF0000":"#000000";
					cuerpoCorreo += 
							"<tr style=\"color:"+color+";\">"+    
									"<td>" + pymeRFC + "</td>" +
									"<td>" + regTablaErrores.getString("DOC_PROCESADO") + 
									"<td>" +regTablaErrores.getString("DFECHA_CREA_EXPED") + "</td>" + 
									"<td>" + regTablaErrores.getString("DINICIO_DOCTO") + "</td>" +
									"<td>" + regTablaErrores.getString("DFIN_DOCTO") +"</td>" +
									"<td>" + new DecimalFormat("#.##").format(iTamDoc) + " MB</td>" + 
									"<td>" + user + "</td>" +
									"<td>" + regTablaErrores.getString("DESCRIPCION") +"</td>" + 
									"</tr>";


				}
				cuerpoCorreo += "</table>";
				cuerpoCorreo += 
						"<br><br>" +
								"<font style='font-size:9.0pt;font-family:Arial,sans-serif'>" +
								"<b>Nota: Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar " + 
								"correos electrónicos de entrada. Por favor no respondas a este mensaje</b></font>" +
								"</body></html>";


				Correo correo = new Correo();
				correo.setNoResponseSender();
				//correo.enviaCorreo(mail, "Ejecución Arma Expediente", cuerpoCorreo);
				
				if (existErrores == false) 
					log.info("Manda correo sin errores para el RFC:" + pymeRFC + 
							"  nombre del documento: " + numeroNE + ".pdf");	
				else
					log.info("Manda correo SI HUBO errores para el RFC:" + pymeRFC + 
							"  nombre del documento: " + numeroNE + ".pdf");
			}


			exito=true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			con.terminaTransaccion(exito);
		}

	}

	private String getCorreosAdmin(AccesoDB con)throws IOException,Exception {
		String SqlCorreo="select vvalor as CORREO from ECON_SYS_PARAMETROS where VNOMBRE_PARAMETRO in('EMAIL_ADMIN','EMAIL_SISTEMAS')";
		Registros regCorreo = con.consultarDB(SqlCorreo);
		String correos="";
		while(regCorreo.next())
		{
			correos+= (regCorreo.getString("CORREO")+",");
		}
		return correos;
	}
	private String getConvenioUnico(String RFC,AccesoDB con) throws Exception
	{
		String banderaConvenioUnico="";
		String query="SELECT CS_CONVENIO_UNICO FROM COMCAT_PYME WHERE CG_RFC ='"+RFC+"'";
		Registros regConvenio = con.consultarDB(query);
		if(regConvenio.next())
			banderaConvenioUnico = regConvenio.getString("CS_CONVENIO_UNICO");

		return banderaConvenioUnico;
	}

	
}
