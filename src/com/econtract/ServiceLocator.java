package com.econtract;

import java.io.PrintStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

public class ServiceLocator
{
  private Context initialContext;
  private Map<String, DataSource> cache;
  private static ServiceLocator serviceLocatorRef = null;
  
  private ServiceLocator()
  {
    try
    {
      this.initialContext = new InitialContext();
      this.cache = Collections.synchronizedMap(new HashMap());
      
      initLog4J();
    }
    catch (NamingException ex)
    {
      throw new AppException("Error al inicializar ServiceLocator", ex);
    }
  }
  
  public static ServiceLocator getInstance()
  {
    if (serviceLocatorRef == null) {
      serviceLocatorRef = new ServiceLocator();
    }
    return serviceLocatorRef;
  }
  
  public DataSource getDataSource(String dataSourceName)
  {
    DataSource datasource = null;
    try
    {
      if (this.cache.containsKey(dataSourceName))
      {
        datasource = (DataSource)this.cache.get(dataSourceName);
      }
      else
      {
        datasource = (DataSource)this.initialContext.lookup(dataSourceName);
        this.cache.put(dataSourceName, datasource);
      }
      return datasource;
    }
    catch (NamingException ex)
    {
      throw new AppException("Error al obtener el DataSource", ex);
    }
  }
  
  public Log getLog(Class clase)
  {
    return LogFactory.getLog(clase);
  }
  
  public Log getLog(String clase)
  {
    return LogFactory.getLog(clase);
  }
  
  private void initLog4J()
  {
    try
    {
      Properties propLog4J = Comunes.loadParams("log4j");
      PropertyConfigurator.configure(propLog4J);
    }
    catch (Throwable t)
    {
      System.out.println("*** El archivo de configuracion log4j.properties no se encontro. Inicializando log4j con BasicConfigurator");
      BasicConfigurator.configure();
    }
  }
  
  public Context getInitialContext()
  {
    return this.initialContext;
  }
}
