package com.econtract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class Registros
  implements Serializable
{
  private List colnames;
  private List data;
  int numRegistro;
  
  public Registros()
  {
    $init$();
  }
  
  public Registros(List colNames)
  {
    $init$();
    setNombreColumnas(colNames);
  }
  
  public List getNombreColumnas()
  {
    return this.colnames;
  }
  
  public void setNombreColumnas(List colnames)
  {
    this.colnames = colnames;
    for (int i = 0; i < this.colnames.size(); i++) {
      this.colnames.set(i, this.colnames.get(i).toString().toUpperCase());
    }
  }
  
  public void setData(List data)
  {
    this.data = (data.isEmpty() ? null : data);
  }
  
  public String getJSONData()
  {
    JSONArray jsonArr = new JSONArray();
    while (next())
    {
      JSONObject jsonObj = new JSONObject();
      for (int i = 0; i < this.colnames.size(); i++) {
        jsonObj.put(this.colnames.get(i), getObject(i + 1));
      }
      jsonArr.add(jsonObj);
    }
    return jsonArr.toString();
  }
  
  public List getData()
  {
    return this.data;
  }
  
  public List getData(int indice)
  {
    return (List)this.data.get(indice);
  }
  
  public void addDataRow(List datarow)
  {
    if (this.data != null)
    {
      this.data.add(datarow);
    }
    else
    {
      this.data = new ArrayList();
      this.data.add(datarow);
    }
  }
  
  public String getString(String nombreCampo)
  {
    int indice = this.colnames.indexOf(nombreCampo.toUpperCase());
    if (indice == -1) {
      throw new RuntimeException("El campo " + nombreCampo + " no existe");
    }
    List renglon = (List)this.data.get(this.numRegistro);
    
    return renglon == null ? null : renglon.get(indice).toString();
  }
  
  public String getString(int posicion)
  {
    int indice = posicion - 1;
    List renglon = (List)this.data.get(this.numRegistro);
    

    return renglon == null ? null : renglon.get(indice).toString();
  }
  
  public Object getObject(String nombreCampo)
  {
    int indice = this.colnames.indexOf(nombreCampo.toUpperCase());
    if (indice == -1) {
      throw new RuntimeException("El campo " + nombreCampo + " no existe");
    }
    List renglon = (List)this.data.get(this.numRegistro);
    

    return renglon.get(indice);
  }
  
  public Object getObject(int posicion)
  {
    int indice = posicion - 1;
    List renglon = (List)this.data.get(this.numRegistro);
    

    return renglon.get(indice);
  }
  
  public void setObject(String nombreCampo, Object value)
  {
    int indice = this.colnames.indexOf(nombreCampo.toUpperCase());
    if (indice == -1) {
      throw new RuntimeException("El campo " + nombreCampo + " no existe");
    }
    List renglon = (List)this.data.get(this.numRegistro);
    
    renglon.set(indice, value);
  }
  
  public void setObject(int posicion, Object value)
  {
    int indice = posicion - 1;
    List renglon = (List)this.data.get(this.numRegistro);
    
    renglon.set(indice, value);
  }
  
  public boolean next()
  {
    boolean haySiguiente = false;
    if ((this.data != null) && (this.data.size() > 0))
    {
      this.numRegistro += 1;
      if (this.numRegistro < this.data.size())
      {
        haySiguiente = true;
      }
      else
      {
        haySiguiente = false;
        this.numRegistro = -1;
      }
    }
    return haySiguiente;
  }
  
  public void rewind()
  {
    this.numRegistro = -1;
  }
  
  public int getNumeroRegistros()
  {
    return this.data == null ? 0 : this.data.size();
  }
  
  private void $init$()
  {
    this.numRegistro = -1;
  }
  
  public void setNumRegistro(int numRegistro)
  {
    this.numRegistro = numRegistro;
  }
  
  public int getNumRegistro()
  {
    return this.numRegistro;
  }
  
  public void remove(int numeroRegistro)
  {
    this.data.remove(numeroRegistro);
  }
  
  public void remove()
  {
    this.data.remove(this.numRegistro);
  }
}
