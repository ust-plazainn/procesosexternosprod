package com.econtract;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DataRow
  implements Serializable
{
  private Map dataRow;
  private ArrayList fields;
  
  public DataRow()
  {
    this.dataRow = Collections.synchronizedMap(new HashMap());
    this.fields = new ArrayList();
  }
  
  public void addData(String key, Object value)
  {
    this.dataRow.put(key, value);
    this.fields.add(key);
  }
  
  public void removeData(String key, Object value)
  {
    this.dataRow.remove(value);
    this.fields.remove(key);
  }
  
  public Object getData(String key)
  {
    return this.dataRow.get(key);
  }
  
  public String getString(String key)
  {
    return "" + this.dataRow.get(key);
  }
  
  public int getFieldCount()
  {
    return this.fields.size();
  }
  
  public ArrayList getFieldNames()
  {
    return this.fields;
  }
  
  public void close()
  {
    if (this.dataRow != null) {
      this.dataRow.clear();
    }
    if (this.fields != null) {
      this.fields.clear();
    }
    this.dataRow = null;
    this.fields = null;
  }
}
