package com.econtract;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;

public class Comunes
{
  public static String truncaCantidad(double cantidadTruncar, int decimales)
  {
    double potencia = Math.pow(10.0D, decimales);
    cantidadTruncar *= potencia;
    
    cantidadTruncar = Math.floor(cantidadTruncar);
    
    cantidadTruncar /= 100;
    
    return String.valueOf(cantidadTruncar);
  }
  
  public static double truncaCantidadDouble(double cantidadTruncar, int decimales)
  {
    return Double.parseDouble(truncaCantidad(cantidadTruncar, decimales));
  }
  
  public static String formatoDecimal(float flotante, int decimales, boolean agrupar)
  {
    String strFlotante = String.valueOf(flotante);
    
    return formatoDecimal(strFlotante, decimales, agrupar);
  }
  
  public static String formatoDecimal(double doble, int decimales, boolean agrupar)
  {
    String strDoble = String.valueOf(doble);
    
    return formatoDecimal(strDoble, decimales, agrupar);
  }
  
  public static String formatoDecimal(Object obj, int decimales, boolean agrupar)
  {
    String numero = "";
    String numeroFormato = "";
    if (obj != null) {
      numero = obj.toString();
    } else {
      numero = "0";
    }
    if (numero.trim().equals("")) {
      numero = "0";
    }
    NumberFormat nf = NumberFormat.getNumberInstance(new Locale("en", "US"));
    nf.setGroupingUsed(agrupar);
    nf.setMaximumFractionDigits(decimales);
    nf.setMinimumFractionDigits(decimales);
    try
    {
      numeroFormato = nf.format(Double.parseDouble(numero.trim()));
    }
    catch (NumberFormatException e)
    {
      numeroFormato = "";
    }
    return numeroFormato;
  }
  
  public static String formatoDecimal(Object obj, int decimales)
  {
    return formatoDecimal(obj, decimales, true);
  }
  
  public static String formatoDecimalSC(Object obj, int decimales)
  {
    String valor = formatoDecimal(obj, decimales, true);
    String digito = "";
    int poss = 0;
    for (int i = 0; i < valor.length(); i++)
    {
      digito = valor.substring(i, i + 1);
      if (digito.equals(","))
      {
        poss = i;
        valor = valor.substring(0, poss - 1).concat(valor.substring(poss + 1, valor.length()));
      }
    }
    return valor;
  }
  
  public static String formatoDecimal(float flotante, int decimales)
  {
    return formatoDecimal(flotante, decimales, true);
  }
  
  public static String formatoDecimal(double doble, int decimales)
  {
    return formatoDecimal(doble, decimales, true);
  }
  
  public static String rellenaCeros(String numero, int numDigitos)
  {
    String tmpNumero = new String(numero);
    for (int i = 1; i <= numDigitos - numero.length(); i++) {
      tmpNumero = "0" + tmpNumero;
    }
    return tmpNumero;
  }
  
  public static String calculoFolio(int numero, int numDigitos, String esProducto)
  {
    String lsNumero = "";
    if (esProducto.equals("5"))
    {
      lsNumero = rellenaCeros(String.valueOf(numero), numDigitos);
      lsNumero = lsNumero + "5";
      


      return lsNumero;
    }
    return calculaFolio(String.valueOf(numero), numDigitos, false);
  }
  
  public static String calculaFolio(String numero, int numDigitos, String esProducto)
  {
    String lsNumero = "";
    if (esProducto.equals("5"))
    {
      lsNumero = rellenaCeros(numero, numDigitos);
      lsNumero = lsNumero + "5";
      


      return lsNumero;
    }
    return calculaFolio(numero, numDigitos, false);
  }
  
  public static String calculaFolio(int numero, int numDigitos)
  {
    return calculaFolio(String.valueOf(numero), numDigitos, false);
  }
  
  public static String calculaFolio(String numero, int numDigitos)
  {
    return calculaFolio(numero, numDigitos, false);
  }
  
  public static String calculaFolio(String numero, int numDigitos, boolean separador)
  {
    Integer[] numArray = new Integer[numDigitos];
    int digitoVerificador = 0;
    int j = numDigitos;
    
    numero = rellenaCeros(numero, numDigitos);
    for (int i = 0; i < numDigitos; i++)
    {
      j--;
      numArray[i] = new Integer(Integer.parseInt(numero.charAt(i) + "") + j);
    }
    for (int i = 0; i < numDigitos; i++) {
      digitoVerificador += numArray[i].intValue();
    }
    if (separador == true) {
      return numero + "-" + digitoVerificador % 10;
    }
    return numero + digitoVerificador % 10;
  }
  
  public static String formatoMoneda2(String numero, boolean signo)
  {
    String numeroFormato = "";
    if (numero == null) {
      numero = "";
    }
    if (!numero.trim().equals(""))
    {
      NumberFormat nf = NumberFormat.getNumberInstance(new Locale("en", "US"));
      
      nf.setMaximumFractionDigits(2);
      nf.setMinimumFractionDigits(2);
      try
      {
        numeroFormato = nf.format(Double.parseDouble(numero.trim()));
        if (signo) {
          numeroFormato = "$ " + numeroFormato;
        }
      }
      catch (NumberFormatException e)
      {
        numeroFormato = "";
      }
    }
    return numeroFormato;
  }
  
  public static String formatoMN(String cifra)
  {
    NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("es", "MX"));
    try
    {
      return nf.format(Double.parseDouble(cifra));
    }
    catch (NumberFormatException nfe)
    {
      return "";
    }
  }
  
  public static String quitaComitasSimples(String cadena)
  {
    String palabra = cadena;
    String letra = "";
    int pf = 1;
    StringBuffer resultado = new StringBuffer();
    for (int i = 0; i < palabra.length(); i++)
    {
      letra = palabra.substring(i, pf++);
      if (!letra.equals("'")) {
        resultado.append(letra);
      }
    }
    return resultado.toString().trim();
  }
  
  public static String quitaComitasSimplesyDobles(String cadena)
  {
    String palabra = cadena;
    String letra = "";
    int pf = 1;
    StringBuffer resultado = new StringBuffer();
    for (int i = 0; i < palabra.length(); i++)
    {
      letra = palabra.substring(i, pf++);
      if ((!letra.equals("'")) && (!letra.equals("\""))) {
        resultado.append(letra);
      }
    }
    return resultado.toString().trim();
  }
  
  public static boolean esNumero(String numero)
  {
    boolean isNum = false;
    try
    {
      BigInteger num = new BigInteger(numero);
      isNum = true;
    }
    catch (NumberFormatException nfe)
    {
      isNum = false;
    }
    return isNum;
  }
  
  public static boolean checaFecha(String formato)
  {
    String date = formato;
    StringTokenizer st = new StringTokenizer(date, "/");
    int dia = 0;int mes = 0;int ano = 0;
    boolean isFecha = false;
    while (st.hasMoreTokens()) {
      try
      {
        dia = Integer.parseInt(st.nextToken());
        mes = Integer.parseInt(st.nextToken()) - 1;
        ano = Integer.parseInt(st.nextToken());
        
        Calendar fecha = new GregorianCalendar(ano, mes, dia);
        int year = fecha.get(1);
        int month = fecha.get(2);
        int day = fecha.get(5);
        if ((dia != day) || (mes != month) || (ano != year)) {
          isFecha = false;
        } else {
          isFecha = true;
        }
      }
      catch (NoSuchElementException nse)
      {
        isFecha = false;
      }
      catch (NumberFormatException nf)
      {
        isFecha = false;
      }
    }
    return isFecha;
  }
  
  public static Date parseDate(String formato)
  {
    StringTokenizer st = new StringTokenizer(formato, "/");
    int dia = 0;int mes = 0;int ano = 0;
    dia = Integer.parseInt(st.nextToken());
    mes = Integer.parseInt(st.nextToken());
    ano = Integer.parseInt(st.nextToken());
    Calendar fecha = Calendar.getInstance();
    Date fecIni = new Date();
    fecIni.setTime(0L);
    fecha.setTime(fecIni);
    fecha.set(ano, mes - 1, dia, 0, 0, 0);
    
    return fecha.getTime();
  }
  
  public static boolean esDecimal(String numero)
  {
    boolean isNum = false;
    try
    {
      BigDecimal num = new BigDecimal(numero.trim());
      isNum = true;
    }
    catch (NumberFormatException nfe)
    {
      isNum = false;
    }
    return isNum;
  }
  
  public static List explode(String separador, String cadena, Class clase)
  {
    List vDatos = new ArrayList();
    if (cadena == null) {
      return vDatos;
    }
    StringTokenizer st = new StringTokenizer(cadena, separador);
    int i = 0;
    while (st.hasMoreTokens())
    {
      Object obj = null;
      String valor = st.nextToken().trim();
      if (clase.getName().equals("java.lang.Integer"))
      {
        if ((valor == null) || (valor.equals(""))) {
          obj = Integer.class;
        } else {
          obj = new Integer(valor);
        }
      }
      else if (clase.getName().equals("java.lang.Long"))
      {
        if ((valor == null) || (valor.equals(""))) {
          obj = Long.class;
        } else {
          obj = new Long(valor);
        }
      }
      else if (clase.getName().equals("java.lang.Double"))
      {
        if ((valor == null) || (valor.equals(""))) {
          obj = Double.class;
        } else {
          obj = new Double(valor);
        }
      }
      else if (clase.getName().equals("java.lang.Float"))
      {
        if ((valor == null) || (valor.equals(""))) {
          obj = Float.class;
        } else {
          obj = new Float(valor);
        }
      }
      else if (clase.getName().equals("java.math.BigInteger"))
      {
        if ((valor == null) || (valor.equals(""))) {
          obj = BigInteger.class;
        } else {
          obj = new BigInteger(valor);
        }
      }
      else if (clase.getName().equals("java.math.BigDecimal"))
      {
        if ((valor == null) || (valor.equals(""))) {
          obj = BigDecimal.class;
        } else {
          obj = new BigDecimal(valor);
        }
      }
      else {
        obj = String.valueOf(valor);
      }
      vDatos.add(i, obj);
      i++;
    }
    return vDatos;
  }
  
  /**
   * @deprecated
   */
  public static List explode(String separador, String cadena, String clase)
  {
    List vDatos = new ArrayList();
    if (cadena == null) {
      return vDatos;
    }
    StringTokenizer st = new StringTokenizer(cadena, separador);
    int i = 0;
    while (st.hasMoreTokens())
    {
      String valor = st.nextToken().trim();
      if (clase.equals("java.lang.Integer"))
      {
        Integer integer = null;
        integer = (valor == null) || (valor.equals("")) ? null : new Integer(valor);
        
        vDatos.add(i, integer);
      }
      else
      {
        vDatos.add(i, valor);
      }
      i++;
    }
    return vDatos;
  }
  
  public static String implode(String separador, List unir)
  {
    StringBuffer resultado = new StringBuffer();
    for (int i = 0; i < unir.size(); i++) {
      if (i == 0) {
        resultado.append((String)unir.get(i));
      } else {
        resultado.append(separador + (String)unir.get(i));
      }
    }
    return resultado.toString();
  }
  
  public static String implode(String separador, String[] unirArreglo)
  {
    StringBuffer resultado = new StringBuffer();
    for (int i = 0; i < unirArreglo.length; i++) {
      if ((unirArreglo[i] != null) && (!unirArreglo[i].equals(""))) {
        if (resultado.length() == 0) {
          resultado.append(unirArreglo[i]);
        } else {
          resultado.append(separador + unirArreglo[i]);
        }
      }
    }
    return resultado.toString();
  }
  
  public static String ordenarCampos(String[] orden, String[] campos)
  {
    String cadena = "";
    Hashtable ht = new Hashtable();
    for (int i = 0; i < campos.length; i++) {
      if (!orden[i].trim().equals("")) {
        ht.put(orden[i], campos[i]);
      }
    }
    Object[] llaves = ht.keySet().toArray();
    Arrays.sort(llaves);
    for (int i = 0; i < llaves.length; i++) {
      if (i == 0) {
        cadena = (String)ht.get(llaves[i]);
      } else {
        cadena = cadena + "," + ht.get(llaves[i]);
      }
    }
    return cadena;
  }
  
  public static String checaVacio(String valor)
  {
    String resultado = "";
    if (valor.length() == 0) {
      resultado = "NULL";
    } else {
      resultado = valor;
    }
    return resultado;
  }
  
  public static String generapassword()
  {
    StringBuffer sb = new StringBuffer();
    String[] numerico = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
    String[] alfabetico = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z" };
    

    String[] alfanumerico = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
    



    int rnd = new Integer((int)(Math.random() * alfabetico.length)).intValue();
    
    sb.append(alfabetico[rnd]);
    for (int i = 0; i < 6; i++)
    {
      rnd = new Integer((int)(Math.random() * alfanumerico.length)).intValue();
      
      sb.append(alfanumerico[rnd]);
    }
    rnd = new Integer((int)(Math.random() * numerico.length)).intValue();
    sb.append(numerico[rnd]);
    

    return sb.toString();
  }
  
  public static String cadenaAleatoria(int longitud)
  {
    String[] x = { "9", "a", "Z", "8", "b", "Y", "7", "c", "X", "6", "d", "W", "5", "e", "V", "4", "f", "U", "3", "g", "T", "2", "h", "S", "1", "i", "R", "0", "j", "Q", "9", "k", "P", "8", "l", "O", "7", "m", "N", "6", "n", "M", "5", "o", "L", "4", "p", "K", "3", "q", "J", "2", "r", "I", "1", "s", "H", "9", "t", "G", "8", "u", "F", "7", "v", "E", "6", "w", "D", "5", "x", "C", "4", "y", "B", "3", "z", "A", "2", "a", "Z", "1", "b", "Y", "0", "c", "X", "9", "d", "W", "8", "e", "V", "7", "f", "U", "6", "g", "T", "5" };
    






    StringBuffer sb = new StringBuffer();
    for (int a = 1; a <= longitud; a++)
    {
      int i = (int)(Math.random() * 100.0D + 1.0D);
      sb.append(x[(i - 1)]);
    }
    return sb.toString();
  }
  
  public static String runCmd(String cmd)
  {
    try
    {
      Runtime rt = Runtime.getRuntime();
      Process p = rt.exec(cmd);
      boolean bOk = false;
      boolean bOkErr = false;
      InputStreamReader in = new InputStreamReader(p.getInputStream());
      InputStreamReader inErr = new InputStreamReader(p.getErrorStream());
      
      BufferedReader reader = new BufferedReader(in);
      BufferedReader readerErr = new BufferedReader(inErr);
      StringBuffer buf = new StringBuffer();
      StringBuffer bufErr = new StringBuffer();
      String line = "";
      String newline = "\n";
      while ((line = reader.readLine()) != null)
      {
        bOk = true;
        buf.append(line);
        buf.append(newline);
      }
      reader.close();
      while ((line = readerErr.readLine()) != null)
      {
        bOkErr = true;
        bufErr.append(line);
        bufErr.append(newline);
      }
      readerErr.close();
      
      p.getInputStream().close();
      p.getOutputStream().close();
      p.getErrorStream().close();
      p.waitFor();
      if (bOk) {
        return buf.toString();
      }
      if (bOkErr) {
        return bufErr.toString();
      }
      return "Desconocido";
    }
    catch (Exception e)
    {
      return e.getMessage();
    }
  }
  
  public static String formatoFijo(String cadena, int cifras, String caracter, String tipo)
  {
    String strCadena = "";
    String strCeros = "";
    String strContenido = "";
    String strTipo = "";
    int i = 0;
    int a = 0;
    
    strContenido = cadena == null ? "" : cadena;
    strTipo = tipo == null ? "" : tipo;
    if (strContenido.length() >= cifras)
    {
      strCadena = strContenido.substring(0, cifras);
    }
    else
    {
      a = cifras - strContenido.length();
      for (i = 1; i <= a; i++) {
        strCeros = strCeros + caracter;
      }
      if ((strTipo.equals("A")) || (strTipo.equals("a"))) {
        strCadena = strCadena + strContenido + strCeros;
      } else {
        strCadena = strCadena + strCeros + strContenido;
      }
    }
    return strCadena;
  }
  
  public static String rellenaCadena(String campo, int longitud)
  {
    char[] cadena = new char[longitud];
    Arrays.fill(cadena, ' ');
    if (campo == null) {
      campo = "";
    }
    int tam = campo.length() > longitud ? longitud : campo.length();
    
    String cadenaTemp = campo.substring(0, tam);
    System.arraycopy(cadenaTemp.toCharArray(), 0, cadena, 0, cadenaTemp.length());
    

    return new String(cadena);
  }
  
  public static String repetirCadenaConSeparador(String cadenaARepetir, String separador, int numeroDeRepeticiones)
  {
    StringBuffer sb = new StringBuffer();
    for (int i = 1; i <= numeroDeRepeticiones; i++) {
      sb.append(separador + cadenaARepetir);
    }
    return sb.toString();
  }
  
  public static String rellenaNumero(String numero, int enteros, int decimales)
  {
    String numeroFormato = "";
    if (numero == null) {
      numero = "";
    }
    NumberFormat nf = NumberFormat.getNumberInstance(new Locale("en", "US"));
    nf.setGroupingUsed(false);
    nf.setMaximumIntegerDigits(enteros);
    nf.setMinimumIntegerDigits(enteros);
    nf.setMaximumFractionDigits(decimales);
    nf.setMinimumFractionDigits(decimales);
    try
    {
      numeroFormato = nf.format(Double.parseDouble(numero.trim()));
    }
    catch (NumberFormatException e)
    {
      numeroFormato = "";
    }
    return strtr(numeroFormato, ".", "");
  }
  
  public static String strtr(String cadena, String separador, String nuevoSeparador)
  {
    String cadenaResultante = "";
    if (cadena == null) {
      cadena = "";
    }
    StringTokenizer st = new StringTokenizer(cadena, separador);
    while (st.hasMoreTokens()) {
      cadenaResultante = cadenaResultante + st.nextToken().trim() + nuevoSeparador;
    }
    return cadenaResultante;
  }
  
  public static String protegeCaracter(String cadena, char caracterProteger, String protector)
  {
    StringBuffer sb = new StringBuffer();
    sb.append(cadena);
    for (int i = 0; i < sb.length(); i++) {
      if (sb.charAt(i) == caracterProteger)
      {
        sb.insert(i, protector);
        i += protector.length();
      }
    }
    return sb.toString();
  }
  
  public static String reemplaza(String psWord, String psReplace, String psNewSeg)
  {
    StringBuffer lsNewStr = new StringBuffer();
    int liFound = 0;
    int liLastPointer = 0;
    do
    {
      liFound = psWord.indexOf(psReplace, liLastPointer);
      if (liFound < 0)
      {
        lsNewStr.append(psWord.substring(liLastPointer, psWord.length()));
      }
      else
      {
        if (liFound > liLastPointer) {
          lsNewStr.append(psWord.substring(liLastPointer, liFound));
        }
        lsNewStr.append(psNewSeg);
        liLastPointer = liFound + psReplace.length();
      }
    } while (liFound > -1);
    return lsNewStr.toString();
  }
  
  public static List getListTokenizer(String linea, String delimitador)
  {
    String elem = "";String cad = "";
    int possIni = 0;int possFin = 0;int countokens = 0;
    List list = new ArrayList();
    try
    {
      if (linea.indexOf(delimitador) == -1)
      {
        if (!linea.trim().equals("")) {
          list.add(linea);
        }
      }
      else
      {
        for (int i = 0; i < linea.length(); i++)
        {
          elem = linea.substring(i, i + 1);
          if (elem.equals(delimitador))
          {
            possIni = countokens == 0 ? 0 : possFin;
            possFin = i + 1;
            countokens++;
            cad = linea.substring(possIni, possFin - 1);
            
            list.add(cad);
          }
        }
        String Delimit1 = linea.substring(possFin - 1, possFin);
        String Delimit2 = linea.substring(linea.length() - 1, linea.length());
        if ((Delimit1.equals(delimitador)) && (!Delimit2.equals(delimitador)))
        {
          if (possFin < linea.length()) {
            list.add(linea.substring(possFin, linea.length()));
          }
        }
        else if (Delimit2.equals(delimitador)) {
          list.add("");
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      list = new ArrayList();
    }
    return list;
  }
  
  public static Registros getTokensArchivo(String path, String delimiter, List layout)
  {
    Registros registros = new Registros(layout);
    try
    {
      File file = new File(path);
      BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
      
      String line = "";
      while ((line = bufferedreader.readLine()) != null)
      {
        line = line.replace('\'', ' ').replace('"', ' ').replace('\\', ' ').trim();
        
        registros.addDataRow(getListTokenizer(line, delimiter));
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return registros;
  }
  
  public static String getExtensionNombreArchivo(String nombreArchivo)
  {
    try
    {
      if ((nombreArchivo == null) || (nombreArchivo.trim().equals(""))) {
        throw new Exception("Los parametros son requeridos");
      }
    }
    catch (Exception e)
    {
      throw new IllegalArgumentException("getExtensionNombreArchivo()::Los parametros recibidos no son validos. " + e.getMessage() + "\n" + "nombreArchivo=" + nombreArchivo);
    }
    List elementosArchivo = explode(".", nombreArchivo, String.class);
    
    String extension = elementosArchivo.size() > 0 ? (String)elementosArchivo.get(elementosArchivo.size() - 1) : "";
    



    return extension.toLowerCase();
  }
  
  public static boolean esAlfabetico(String str)
  {
    char caracter = '\000';
    if ((str == null) || (str.length() == 0)) {
      return false;
    }
    for (int i = 0; i < str.length(); i++)
    {
      caracter = str.charAt(i);
      if (((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && (caracter != 'Ñ') && (caracter != 'ñ')) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean esAlfabetico(String str, String extraChars)
  {
    char caracter = '\000';
    if ((str == null) || (str.length() == 0)) {
      return false;
    }
    if (extraChars == null) {
      return esAlfabetico(str);
    }
    for (int i = 0; i < str.length(); i++)
    {
      caracter = str.charAt(i);
      if (((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && (caracter != 'Ñ') && (caracter != 'ñ') && (extraChars.indexOf(caracter) == -1)) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean esAlfanumerico(String str)
  {
    char caracter = '\000';
    String caracteresEsp = "ñÑ";
    if ((str == null) || (str.length() == 0)) {
      return false;
    }
    for (int i = 0; i < str.length(); i++)
    {
      caracter = str.charAt(i);
      if (((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) && (caracter != 'Ñ') && (caracter != 'ñ')) {
        return false;
      }
    }
    return true;
  }
  
  public static boolean esAlfanumerico(String str, String extraChars)
  {
    char caracter = '\000';
    if ((str == null) || (str.length() == 0)) {
      return false;
    }
    if (extraChars == null) {
      return esAlfabetico(str);
    }
    for (int i = 0; i < str.length(); i++)
    {
      caracter = str.charAt(i);
      if (((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) && (caracter != 'Ñ') && (caracter != 'ñ') && (extraChars.indexOf(caracter) == -1)) {
        return false;
      }
    }
    return true;
  }
  
  public static String suprimeCaracteres(String str, String caracteres)
  {
    char caracter = '\000';
    StringBuffer resultado = new StringBuffer();
    if ((str == null) || (caracteres == null)) {
      return str;
    }
    for (int i = 0; i < str.length(); i++)
    {
      caracter = str.charAt(i);
      if (caracteres.indexOf(caracter) == -1) {
        resultado.append(caracter);
      }
    }
    return resultado.toString();
  }
  
  public static boolean esComparacionNumericaValida(String sNumero, String operador, String sNumero2)
  {
    BigDecimal numero = null;
    BigDecimal numero2 = null;
    try
    {
      if ((sNumero == null) || (sNumero.trim().equals("")) || (operador == null) || (operador.trim().equals("")) || (sNumero2 == null) || (sNumero2.trim().equals(""))) {
        throw new Exception("Los parametros son requeridos");
      }
      if ((!operador.equalsIgnoreCase("eq")) && (!operador.equalsIgnoreCase("ne")) && (!operador.equalsIgnoreCase("gt")) && (!operador.equalsIgnoreCase("ge")) && (!operador.equalsIgnoreCase("lt")) && (!operador.equalsIgnoreCase("le"))) {
        throw new Exception("El operador no es valido");
      }
      numero = new BigDecimal(sNumero);
      numero2 = new BigDecimal(sNumero2);
    }
    catch (Exception e)
    {
      throw new IllegalArgumentException("Los parametros recibidos no son validos. " + e.getMessage() + "\n" + "sNumero=" + sNumero + "\n" + "operador=" + operador + "\n" + "sNumero2=" + sNumero2);
    }
    boolean resultado = false;
    if (operador.equalsIgnoreCase("eq")) {
      resultado = numero.compareTo(numero2) == 0;
    } else if (operador.equalsIgnoreCase("ne")) {
      resultado = numero.compareTo(numero2) != 0;
    } else if (operador.equalsIgnoreCase("gt")) {
      resultado = numero.compareTo(numero2) > 0;
    } else if (operador.equalsIgnoreCase("ge")) {
      resultado = numero.compareTo(numero2) >= 0;
    } else if (operador.equalsIgnoreCase("lt")) {
      resultado = numero.compareTo(numero2) < 0;
    } else if (operador.equalsIgnoreCase("le")) {
      resultado = numero.compareTo(numero2) <= 0;
    }
    return resultado;
  }
  
  public static boolean esComparacionFechaValida(String sFecha, String operador, String sFecha2, String formatoFecha)
  {
    Date fecha = null;
    Date fecha2 = null;
    try
    {
      if ((sFecha == null) || (sFecha.trim().equals("")) || (operador == null) || (operador.trim().equals("")) || (sFecha2 == null) || (sFecha2.trim().equals("")) || (formatoFecha == null) || (formatoFecha.trim().equals(""))) {
        throw new Exception("Los parametros son requeridos");
      }
      if ((!operador.equalsIgnoreCase("eq")) && (!operador.equalsIgnoreCase("ne")) && (!operador.equalsIgnoreCase("gt")) && (!operador.equalsIgnoreCase("ge")) && (!operador.equalsIgnoreCase("lt")) && (!operador.equalsIgnoreCase("le"))) {
        throw new Exception("El operador no es valido");
      }
      fecha = new SimpleDateFormat(formatoFecha).parse(sFecha);
      fecha2 = new SimpleDateFormat(formatoFecha).parse(sFecha2);
    }
    catch (Exception e)
    {
      throw new IllegalArgumentException("Los parametros recibidos no son validos. " + e.getMessage() + "\n" + "sFecha=" + sFecha + "\n" + "operador=" + operador + "\n" + "sFecha2=" + sFecha2 + "\n" + "formatoFecha=" + formatoFecha);
    }
    boolean resultado = false;
    if (operador.equalsIgnoreCase("eq")) {
      resultado = fecha.compareTo(fecha2) == 0;
    } else if (operador.equalsIgnoreCase("ne")) {
      resultado = fecha.compareTo(fecha2) != 0;
    } else if (operador.equalsIgnoreCase("gt")) {
      resultado = fecha.compareTo(fecha2) > 0;
    } else if (operador.equalsIgnoreCase("ge")) {
      resultado = fecha.compareTo(fecha2) >= 0;
    } else if (operador.equalsIgnoreCase("lt")) {
      resultado = fecha.compareTo(fecha2) < 0;
    } else if (operador.equalsIgnoreCase("le")) {
      resultado = fecha.compareTo(fecha2) <= 0;
    }
    return resultado;
  }
  
  public static boolean esExtensionArchivoValida(String nombreArchivo, String extensiones)
  {
    boolean resultado = false;
    try
    {
      if ((nombreArchivo == null) || (nombreArchivo.trim().equals("")) || (extensiones == null) || (extensiones.trim().equals(""))) {
        throw new Exception("Los parametros son requeridos");
      }
    }
    catch (Exception e)
    {
      throw new IllegalArgumentException("Los parametros recibidos no son validos. " + e.getMessage() + "\n" + "nombreArchivo=" + nombreArchivo + "\n" + "extensiones=" + extensiones);
    }
    List listaExtensiones = explode(",", extensiones, String.class);
    
    Iterator it = listaExtensiones.iterator();
    while (it.hasNext())
    {
      String extension = ((String)it.next()).toUpperCase();
      if (nombreArchivo.endsWith("." + extension))
      {
        resultado = true;
        break;
      }
    }
    return resultado;
  }
  
  public static String ucFirst(String in)
  {
    if ((in == null) || (in.equals(""))) {
      return in;
    }
    StringBuffer stb = new StringBuffer(in.toLowerCase());
    stb.setCharAt(0, Character.toUpperCase(stb.charAt(0)));
    

    return stb.toString();
  }
  
  public static boolean esNumeroEnteroPositivo(String numero)
  {
    boolean isNum = false;
    try
    {
      BigInteger num = new BigInteger(numero);
      if (num.signum() != -1) {
        isNum = true;
      }
    }
    catch (NumberFormatException nfe)
    {
      isNum = false;
    }
    return isNum;
  }
  
  public static boolean esNumeroEnteroPositivoYDiferenteDeCero(String numero)
  {
    boolean isNum = false;
    try
    {
      BigInteger num = new BigInteger(numero);
      if (num.signum() == 1) {
        isNum = true;
      }
    }
    catch (NumberFormatException nfe)
    {
      isNum = false;
    }
    return isNum;
  }
  
  public static boolean esDecimalPositivo(String numero)
  {
    boolean isNum = false;
    try
    {
      BigDecimal num = new BigDecimal(numero.trim());
      if (num.signum() != -1) {
        isNum = true;
      }
    }
    catch (NumberFormatException nfe)
    {
      isNum = false;
    }
    return isNum;
  }
  
  public static boolean esDecimalPositivoYDiferenteDeCero(String numero)
  {
    boolean isNum = false;
    try
    {
      BigDecimal num = new BigDecimal(numero.trim());
      if (num.signum() == 1) {
        isNum = true;
      }
    }
    catch (NumberFormatException nfe)
    {
      isNum = false;
    }
    return isNum;
  }
  
  public static boolean excedeCantidadDeEnterosYDecimales(String numero, int numEnteros, int numDecimales)
  {
    if ((numero == null) || (numero.equals(""))) {
      return false;
    }
    if (!esDecimal(numero)) {
      return false;
    }
    if (numero.charAt(0) == '-') {
      numero = numero.substring(1, numero.length());
    }
    numEnteros = Math.abs(numEnteros);
    numDecimales = Math.abs(numDecimales);
    String[] datos = null;
    String parteEntera = "";
    String parteDecimal = "";
    
    datos = numero.split("\\.");
    
    parteEntera = datos[0];
    if (datos.length > 1) {
      parteDecimal = datos[1];
    }
    if (parteEntera.length() > numEnteros) {
      return true;
    }
    return parteDecimal.length() > numDecimales;
  }
  
  public static String eliminaAcentos(String strWord)
  {
    char[] strAcentos = { 'á', 'é', 'í', 'ó', 'ú', 'Á', 'É', 'Í', 'Ó', 'Ú' };
    char[] strSinAcen = { 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' };
    if ((strWord != null) && (!strWord.equals(""))) {
      for (int x = 0; x < strAcentos.length; x++) {
        strWord = strWord.replace(strAcentos[x], strSinAcen[x]);
      }
    }
    return strWord;
  }
  
  public static String reemplazarCaracteres(String cadena, String strCharsBuscar, String strCharsReemplazar)
  {
    if ((cadena == null) || (cadena.equals("")) || (strCharsBuscar == null) || (strCharsBuscar.equals(""))) {
      return cadena;
    }
    if (strCharsReemplazar == null) {
      strCharsReemplazar = "";
    }
    boolean modificada = false;
    StringBuffer buf = new StringBuffer(cadena.length());
    for (int i = 0; i < cadena.length(); i++)
    {
      char ch = cadena.charAt(i);
      int index = strCharsBuscar.indexOf(ch);
      if (index >= 0)
      {
        modificada = true;
        if (index < strCharsReemplazar.length()) {
          buf.append(strCharsReemplazar.charAt(index));
        }
      }
      else
      {
        buf.append(ch);
      }
    }
    if (modificada) {
      return buf.toString();
    }
    return cadena;
  }
  
  public static String getCadenaFirmaMozilla(String cadena)
  {
    StringBuffer sbCharsBuscar = new StringBuffer(256);
    StringBuffer sbCharsReemplazar = new StringBuffer(256);
    
    sbCharsBuscar.append("áÁéÉíÍóÓúÚüÜñÑ");
    sbCharsReemplazar.append("aAeEiIoOuUuUnN");
    for (char ch = ''; ch <= 'ÿ'; ch = (char)(ch + '\001')) {
      if (sbCharsBuscar.indexOf(String.valueOf(ch)) == -1)
      {
        sbCharsBuscar.append(ch);
        


        sbCharsReemplazar.append('-');
      }
    }
    return reemplazarCaracteres(cadena, sbCharsBuscar.toString(), sbCharsReemplazar.toString());
  }
  
  public static String getHash(String rutaArchivo)
    throws Exception
  {
    System.out.println("Comunes::getHash(E)");
    
    String hash = "";
    FileInputStream fis = null;
    MessageDigest md = null;
    File f = null;
    try
    {
      f = new File(rutaArchivo);
      fis = new FileInputStream(f);
      md = MessageDigest.getInstance("MD5");
      
      long longitud = f.length();
      
      byte[] bytes = new byte[(int)longitud];
      

      md.reset();
      fis.read(bytes);
      

      md.update(bytes);
      

      byte[] digest = md.digest();
      StringBuffer hexString = new StringBuffer();
      for (int i = 0; i < digest.length; i++)
      {
        String aux = Integer.toHexString(0xFF & digest[i]);
        if (aux.length() == 1) {
          aux = "0" + aux;
        }
        hexString.append(aux);
      }
      hash = hexString.toString();
    }
    catch (Exception e)
    {
      System.out.println("Comunes::getHash(Exception)");
      e.printStackTrace();
      throw e;
    }
    finally
    {
      try
      {
        fis.close();
      }
      catch (Exception localException1) {}
      System.out.println("Comunes::getHash(S)");
    }
    return hash;
  }
  
  public static Properties loadParams(String file)
    throws IOException
  {
    Properties prop = new Properties();
    ResourceBundle bundle = ResourceBundle.getBundle(file);
    

    Enumeration en = bundle.getKeys();
    String key = null;
    while (en.hasMoreElements())
    {
      key = (String)en.nextElement();
      prop.put(key, bundle.getObject(key));
    }
    return prop;
  }
  
  public static boolean esCuentaClabeValida(String cuentaClabe)
  {
    boolean clabeValida = false;
    int digitoVerificador = 0;
    int digitoActual = 0;
    int digitoObtenido = 0;
    int sumaTotal = 0;
    int moduloSumaTotal = 0;
    if ((cuentaClabe.length() == 18) && (esNumeroEnteroPositivo(cuentaClabe)))
    {
      digitoVerificador = Integer.parseInt(cuentaClabe.substring(17));
      for (int i = 0; i < 17; i++)
      {
        digitoActual = Integer.parseInt(cuentaClabe.substring(i, i + 1));
        if ((i == 0) || (i == 3) || (i == 6) || (i == 9) || (i == 12) || (i == 15)) {
          sumaTotal += digitoActual * 3;
        }
        if ((i == 1) || (i == 4) || (i == 7) || (i == 10) || (i == 13) || (i == 16)) {
          sumaTotal += digitoActual * 7;
        }
        if ((i == 2) || (i == 5) || (i == 8) || (i == 11) || (i == 14)) {
          sumaTotal += digitoActual * 1;
        }
      }
      moduloSumaTotal = sumaTotal % 10;
      if (moduloSumaTotal > 0) {
        digitoObtenido = 10 - moduloSumaTotal;
      } else {
        digitoObtenido = 0;
      }
      if (digitoObtenido == digitoVerificador) {
        clabeValida = true;
      }
    }
    return clabeValida;
  }
  
  public static void crearDirectoriosArchivo(String rutaArchivo)
  {
    if ((rutaArchivo != null) && (!rutaArchivo.equals("")))
    {
      String rutaDir = rutaArchivo.substring(0, rutaArchivo.lastIndexOf("/") + 1);
      
      crearDirectorios(rutaDir);
    }
  }
  
  public static void crearDirectorios(String rutaDir)
  {
    if ((rutaDir != null) && (!rutaDir.equals("")))
    {
      File fRuta = new File(rutaDir);
      if (!fRuta.exists()) {
        fRuta.mkdirs();
      }
    }
  }
  
  public static String filtraComas(String numero)
  {
    if ((numero == null) || (numero.trim().equals(""))) {
      return numero;
    }
    return numero.replaceAll(",", "");
  }
  
  public static boolean esUnaLista(String cadena)
  {
    boolean validacion = false;
    if ((cadena != null) && (cadena.indexOf(",") != -1)) {
      validacion = true;
    }
    return validacion;
  }
  
  public static ArrayList stringToArrayList(String lista, String separador)
  {
    ArrayList resultado = null;
    if (lista == null) {
      return new ArrayList();
    }
    if (separador == null) {
      separador = "";
    }
    boolean separadorVacio = separador.equals("");
    
    resultado = new ArrayList(Arrays.asList(lista.split(separador, -1)));
    if ((separadorVacio) && (resultado != null) && (resultado.size() > 1))
    {
      resultado.remove(0);
      if (resultado.size() > 1) {
        resultado.remove(resultado.size() - 1);
      }
    }
    return resultado == null ? new ArrayList() : resultado;
  }
  
  public static String arrayListToString(ArrayList lista, String separador)
  {
    StringBuffer buffer = new StringBuffer();
    if (separador == null) {
      separador = "";
    }
    if (lista == null) {
      lista = new ArrayList();
    }
    for (int i = 0; i < lista.size(); i++)
    {
      if (i > 0) {
        buffer.append(separador);
      }
      buffer.append((String)(lista.get(i) == null ? "" : lista.get(i)));
    }
    return buffer.toString();
  }
  
  public static StringBuffer escapaCaracteresEspeciales(StringBuffer cadena)
  {
    StringBuffer resultado = new StringBuffer();
    if (cadena == null) {
      return null;
    }
    if (cadena.length() == 0) {
      return resultado;
    }
    for (int i = 0; i < cadena.length(); i++)
    {
      char c = cadena.charAt(i);
      if (c == '<') {
        resultado.append("&lt;");
      } else if (c == '>') {
        resultado.append("&gt;");
      } else if (c == '"') {
        resultado.append("&quot;");
      } else if (c == '&') {
        resultado.append("&amp;");
      } else {
        resultado.append(c);
      }
    }
    return resultado;
  }
  
  public static String escapaCaracteresEspeciales(String cadena)
  {
    StringBuffer resultado = new StringBuffer();
    if (cadena == null) {
      return null;
    }
    if (cadena.length() == 0) {
      return resultado.toString();
    }
    for (int i = 0; i < cadena.length(); i++)
    {
      char c = cadena.charAt(i);
      if (c == '<') {
        resultado.append("&lt;");
      } else if (c == '>') {
        resultado.append("&gt;");
      } else if (c == '"') {
        resultado.append("&quot;");
      } else if (c == '&') {
        resultado.append("&amp;");
      } else {
        resultado.append(c);
      }
    }
    return resultado.toString();
  }
  
  public static final String escapeHTML(String s)
  {
    StringBuffer sb = new StringBuffer();
    int n = s.length();
    for (int i = 0; i < n; i++)
    {
      char c = s.charAt(i);
      switch (c)
      {
      case '<': 
        sb.append("&lt;");
        break;
      case '>': 
        sb.append("&gt;");
        break;
      case '&': 
        sb.append("&amp;");
        break;
      default: 
        sb.append(c);
      }
    }
    return sb.toString();
  }
}
