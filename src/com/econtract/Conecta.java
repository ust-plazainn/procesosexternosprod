package com.econtract;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Conecta
{
  public static void main(String[] args)
    throws Exception
  {
    try
    {
      long time_start = System.currentTimeMillis();
      DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
      Date date1 = new Date();
      String fechaInicio = dateFormat.format(date1);
      DocumentosExpediente doc = new DocumentosExpediente();
      System.out.println("*******************INICIA EL PROCESO BATCH " + fechaInicio + "*******************!");
      System.out.println("******************************************************************************");
      System.out.println("******************************************************************************");
      doc.start();
      long time_end = System.currentTimeMillis();
      System.out.println("Fin de proceso " + (time_end - time_start) / 1000L + " segundos");
      System.out.println("Fin proceso!!");
      System.exit(0);
    }
    catch (Throwable e)
    {
      e.printStackTrace();
      System.exit(2);
    }
  }
}
