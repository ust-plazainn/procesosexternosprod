package com.econtract.mail;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.apache.commons.io.output.ByteArrayOutputStream;

public class Correo
{
  String mailHost;
  String mailUser;
  String mailUserCC;
  String mailPassword;
  String mailTransportProtocol;
  String mailSender;
  String mailSenderNoResponse;
  String mailSenderNoResponseName;
  boolean usarCuentaNoResponse;
  
  private void $init$()
  {
    this.mailHost = null;
    this.mailUser = null;
    this.mailUserCC = null;
    this.mailPassword = null;
    this.mailTransportProtocol = null;
    this.mailSender = null;
    this.mailSenderNoResponse = null;
    this.mailSenderNoResponseName = null;
    this.usarCuentaNoResponse = false;
  }
  
  public Correo()
  {
    $init$();
    try
    {
      ResourceBundle bundle = ResourceBundle.getBundle("correo");
      Enumeration enume = bundle.getKeys();
      while (enume.hasMoreElements())
      {
        String key = (String)enume.nextElement();
        if (key.equals("mail.host")) {
          this.mailHost = ((String)bundle.getObject(key));
        } else if (key.equals("mail.user")) {
          this.mailUser = ((String)bundle.getObject(key));
        } else if (key.equals("mail.password")) {
          this.mailPassword = ((String)bundle.getObject(key));
        } else if (key.equals("mail.transport.protocol")) {
          this.mailTransportProtocol = ((String)bundle.getObject(key));
        } else if (key.equals("mail.sender")) {
          this.mailSender = ((String)bundle.getObject(key));
        } else if (key.equals("mail.sender_noresponse")) {
          this.mailSenderNoResponse = ((String)bundle.getObject(key));
        } else if (key.equals("mail.sender_noresponse_name")) {
          this.mailSenderNoResponseName = ((String)bundle.getObject(key));
        }
      }
    }
    catch (Exception e)
    {
      System.out.println("Correo::Correo(Exception) :(");
      e.printStackTrace();
    }
  }
  
  public Correo(String baseName, boolean isBaseName)
  {
    $init$();
    if (!isBaseName) {
      return;
    }
    try
    {
      ResourceBundle bundle = ResourceBundle.getBundle(baseName);
      Enumeration enume = bundle.getKeys();
      while (enume.hasMoreElements())
      {
        String key = (String)enume.nextElement();
        if (key.equals("mail.host")) {
          this.mailHost = ((String)bundle.getObject(key));
        } else if (key.equals("mail.user")) {
          this.mailUser = ((String)bundle.getObject(key));
        } else if (key.equals("mail.password")) {
          this.mailPassword = ((String)bundle.getObject(key));
        } else if (key.equals("mail.transport.protocol")) {
          this.mailTransportProtocol = ((String)bundle.getObject(key));
        } else if (key.equals("mail.sender")) {
          this.mailSender = ((String)bundle.getObject(key));
        } else if (key.equals("mail.sender_noresponse")) {
          this.mailSenderNoResponse = ((String)bundle.getObject(key));
        } else if (key.equals("mail.sender_noresponse_name")) {
          this.mailSenderNoResponseName = ((String)bundle.getObject(key));
        }
      }
    }
    catch (Exception e)
    {
      System.out.println("Correo::Correo(Exception) :(");
      e.printStackTrace();
    }
  }
  
  public void enviaCorreosMultiplesDest(String destinatario, String tituloMensaje, String contenidoMensaje)
  {
    Properties props = new Properties();
    props.put("mail.smtp.host", this.mailHost);
    Session mailSession = null;
    try
    {
      mailSession = Session.getDefaultInstance(props, null);
      mailSession.setDebug(false);
      
      MimeMessage message = new MimeMessage(mailSession);
      
      message.setSubject(tituloMensaje, "ISO-8859-1");
      message.setFrom(new InternetAddress(this.mailSender, this.mailSenderNoResponseName));
      message.setContent(contenidoMensaje, "text/html");
      

      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario, false));
      if ((this.mailUserCC != null) && (!this.mailUserCC.equals("")))
      {
        System.out.println(":::::SE enviar� el correo con CC a:" + this.mailUserCC);
        message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(this.mailUserCC, false));
      }
      else
      {
        System.out.println("NOOO agreg� CC");
      }
      Transport.send(message, message.getAllRecipients());
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally {}
  }
  
  public void enviaCorreo(String destinatario, String tituloMensaje, String contenidoMensaje)
  {
    Properties props = new Properties();
    props.put("mail.smtp.host", this.mailHost);
    Session mailSession = null;
    try
    {
      mailSession = Session.getDefaultInstance(props, null);
      mailSession.setDebug(false);
      
      MimeMessage message = new MimeMessage(mailSession);
      
      message.setSubject(tituloMensaje, "ISO-8859-1");
      if (this.usarCuentaNoResponse) {
        message.setFrom(new InternetAddress(this.mailSender, this.mailSenderNoResponseName));
      } else {
        message.setFrom(new InternetAddress(this.mailSender));
      }
      message.setContent(contenidoMensaje, "text/html; charset=\"UTF-8\"");
      message.addRecipients(Message.RecipientType.TO, destinatario);
      

      //Transport.send(message, message.getRecipients(Message.RecipientType.TO));
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally {}
  }
  
  public void setNoResponseSender()
  {
    this.mailSender = this.mailSenderNoResponse;
    this.usarCuentaNoResponse = true;
  }
  
  public void enviaCorreoConDatosAdjuntos(String destinatario, String tituloMensaje, String contenidoMensaje, ArrayList listaDeArchivosAdjuntos)
  {
    enviaCorreoConDatosAdjuntos(destinatario, tituloMensaje, contenidoMensaje, listaDeArchivosAdjuntos, new ArrayList());
  }
  
  public void enviaCorreoConDatosAdjuntos(String destinatario, String tituloMensaje, String contenidoMensaje, ArrayList listaDeArchivosAdjuntos, ArrayList listaDeArchivosDeMemoriaAdjuntos)
  {
    Properties props = new Properties();
    props.put("mail.smtp.host", this.mailHost);
    Session mailSession = null;
    try
    {
      mailSession = Session.getDefaultInstance(props, null);
      mailSession.setDebug(false);
      

      MimeMessage message = new MimeMessage(mailSession);
      
      message.setSubject(tituloMensaje, "ISO-8859-1");
      if (this.usarCuentaNoResponse) {
        message.setFrom(new InternetAddress(this.mailSender, this.mailSenderNoResponseName));
      } else {
        message.setFrom(new InternetAddress(this.mailSender));
      }
      message.addRecipients(Message.RecipientType.TO, destinatario);
      if ((this.mailUserCC != null) && (!this.mailUserCC.equals(""))) {
        message.addRecipient(Message.RecipientType.CC, new InternetAddress(this.mailUserCC));
      }
      MimeMultipart multipart = new MimeMultipart("related");
      

      BodyPart messageBodyPart = new MimeBodyPart();
      

      messageBodyPart.setContent(contenidoMensaje, "text/html; charset=\"UTF-8\"");
      
      multipart.addBodyPart(messageBodyPart);
      for (int indice = 0; indice < listaDeArchivosAdjuntos.size(); indice++)
      {
        HashMap datos = (HashMap)listaDeArchivosAdjuntos.get(indice);
        messageBodyPart = new MimeBodyPart();
        DataSource fds = new FileDataSource((String)datos.get("FILE_FULL_PATH"));
        messageBodyPart.setDataHandler(new DataHandler(fds));
        messageBodyPart.setHeader("Content-ID", "<" + datos.get("FILE_ID") + ">");
        multipart.addBodyPart(messageBodyPart);
      }
      if (listaDeArchivosDeMemoriaAdjuntos != null) {
        for (int indice = 0; indice < listaDeArchivosDeMemoriaAdjuntos.size(); indice++)
        {
          DataHandler dh = null;
          HashMap datos = (HashMap)listaDeArchivosDeMemoriaAdjuntos.get(indice);
          messageBodyPart = new MimeBodyPart();
          
          Object zipContent = datos.get("FILE_CONTENT");
          if (zipContent.getClass().getName().equals("java.lang.String"))
          {
            dh = new DataHandler(zipContent, (String)datos.get("MIME_TYPE"));
          }
          else
          {
            ByteArrayDataSource btZipObject = new ByteArrayDataSource((byte[])zipContent, (String)datos.get("MIME_TYPE"));
            dh = new DataHandler(btZipObject);
          }
          messageBodyPart.setDataHandler(dh);
          messageBodyPart.setFileName((String)datos.get("FILE_ID"));
          multipart.addBodyPart(messageBodyPart);
        }
      }
      message.setContent(multipart);
      

      Transport.send(message, message.getRecipients(Message.RecipientType.TO));
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally {}
  }
  
  public void enviaCorreoDatoAdjunto(String destinatario, String tituloMensaje, String contenidoMensaje, ByteArrayOutputStream outputStream)
  {
    Properties props = new Properties();
    props.put("mail.smtp.host", this.mailHost);
    Session mailSession = null;
    try
    {
      mailSession = Session.getDefaultInstance(props, null);
      mailSession.setDebug(false);
      
      MimeMessage message = new MimeMessage(mailSession);
      message.setSubject(tituloMensaje, "ISO-8859-1");
      if (this.usarCuentaNoResponse) {
        message.setFrom(new InternetAddress(this.mailSender, this.mailSenderNoResponseName));
      } else {
        message.setFrom(new InternetAddress(this.mailSender));
      }
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario, false));
      if ((this.mailUserCC != null) && (!this.mailUserCC.equals(""))) {
        message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(this.mailUserCC, false));
      }
      MimeMultipart multipart = new MimeMultipart("related");
      BodyPart messageBodyPart = new MimeBodyPart();
      messageBodyPart.setContent(contenidoMensaje, "text/html; charset=\"UTF-8\"");
      multipart.addBodyPart(messageBodyPart);
      
      messageBodyPart = new MimeBodyPart();
      byte[] bytes = outputStream.toByteArray();
      DataSource dataSource = new ByteArrayDataSource(bytes, "application/vnd.ms-excel");
      messageBodyPart.setDataHandler(new DataHandler(dataSource));
      messageBodyPart.setFileName("reporte.xls");
      multipart.addBodyPart(messageBodyPart);
      
      message.setContent(multipart);
      

      Transport.send(message, message.getAllRecipients());
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      System.out.println("enviaCorreoDatosAdjuntos(Exception)::");
    }
    finally
    {
      System.out.println("enviaCorreoDatosAdjuntos(S)");
    }
  }
  
  public String getMailUserCC()
  {
    return this.mailUserCC;
  }
  
  public void setMailUserCC(String mailUserCC)
  {
    this.mailUserCC = mailUserCC;
  }
}
