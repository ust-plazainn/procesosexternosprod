package com.econtract.persistence;

import com.econtract.Comunes;
import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

public class ParametrosGlobalesApp
  implements Serializable
{
  private static ParametrosGlobalesApp singleton = null;
  private static Properties parametros = null;
  
  public static synchronized ParametrosGlobalesApp getInstance()
  {
    if (singleton == null) {
      singleton = new ParametrosGlobalesApp();
    }
    return singleton;
  }
  
  public String getParametro(String clave)
  {
    if (parametros == null) {
      try
      {
        parametros = Comunes.loadParams("accesoDBParam");
      }
      catch (IOException e)
      {
        parametros = new Properties();
      }
    }
    String valorParametro = parametros.getProperty(clave, "");
    
    return valorParametro;
  }
}
