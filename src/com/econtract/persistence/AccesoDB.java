package com.econtract.persistence;

import com.econtract.AccesoDBParam;
import com.econtract.AppException;
import com.econtract.Registros;
import com.econtract.ServiceLocator;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import oracle.jdbc.driver.OracleDriver;
import org.apache.commons.logging.Log;

public class AccesoDB
{
  private DataSource ds;
  private String nameBase;
  
  private void $init$()
  {
    this.nameBase = "";
  }
  
  private static final String DEFAULT_DATASOURCE_NAME = ParametrosGlobalesApp.getInstance().getParametro("DEFAULT_DATASOURCE_NAME");
  private Connection con;
  private static final Log log = ServiceLocator.getInstance().getLog(AccesoDB.class);
  
  public AccesoDB()
  {
    $init$();
  }
  
  public AccesoDB(String base)
  {
    $init$();
    this.nameBase = base;
  }
  
  public void conexionDB()
  {
    try
    {
      if ((this.con != null) && (!this.con.isClosed()))
      {
        log.debug("AccesoDB::conexionDB()::ERROR. Ya hay una conexion Activa");
      }
      else
      {
        String ip = "";
        String puerto = "";
        String servicio = "";
        String usuario = "";
        String password = "";
        if (this.nameBase.equals("ECONTRACT"))
        {
          ip = AccesoDBParam.getParametro("ORACLE_IP_ECON");
          puerto = AccesoDBParam.getParametro("ORACLE_PORT_ECON");
          servicio = AccesoDBParam.getParametro("ORACLE_SERVICE_ECON");
          usuario = AccesoDBParam.getParametro("ORACLE_USER_ECON");
          password = AccesoDBParam.getParametro("ORACLE_PASSWORD_ECON");
        }
        else
        {
          ip = AccesoDBParam.getParametro("ORACLE_IP");
          puerto = AccesoDBParam.getParametro("ORACLE_PORT");
          servicio = AccesoDBParam.getParametro("ORACLE_SERVICE");
          usuario = AccesoDBParam.getParametro("ORACLE_USER");
          password = AccesoDBParam.getParametro("ORACLE_PASSWORD");
        }
        DriverManager.registerDriver(new OracleDriver());
        this.con = DriverManager.getConnection("jdbc:oracle:thin:@" + ip + ":" + puerto + ":" + servicio, usuario, password);
        

        this.con.setAutoCommit(false);
        this.con.setTransactionIsolation(2);
        
        PreparedStatement ps = queryPrecompilado("ALTER SESSION SET NLS_TERRITORY = AMERICA");
        try
        {
          ps.executeUpdate();
        }
        finally
        {
          ps.close();
        }
        this.con.commit();
      }
    }
    catch (Throwable e)
    {
      throw new AppException("Error al establecer una conexion de BD", e);
    }
  }
  
  public PreparedStatement queryPrecompilado(String qrySentencia, List lVarBind, int numMaxRegistros)
  {
    PreparedStatement ps = null;
    try
    {
      ps = this.con.prepareStatement(qrySentencia);
      for (int i = 0; i < lVarBind.size(); i++)
      {
        Object object = lVarBind.get(i);
        String contenido = object.toString().trim();
        if ((object instanceof String)) {
          ps.setString(i + 1, contenido);
        } else if ((object instanceof Integer)) {
          ps.setInt(i + 1, new Integer(contenido).intValue());
        } else if ((object instanceof Long)) {
          ps.setLong(i + 1, new Long(contenido).longValue());
        } else if ((object instanceof Double)) {
          ps.setDouble(i + 1, new Double(contenido).doubleValue());
        } else if ((object instanceof BigDecimal)) {
          ps.setBigDecimal(i + 1, new BigDecimal(contenido));
        } else if ((object instanceof Calendar)) {
          ps.setTimestamp(i + 1, new Timestamp(((Calendar)object).getTime().getTime()), Calendar.getInstance());
        } else if ((object instanceof Date)) {
          ps.setTimestamp(i + 1, new Timestamp(((Date)object).getTime()), Calendar.getInstance());
        } else if ((object instanceof byte[])) {
          ps.setBinaryStream(i + 1, new ByteArrayInputStream((byte[])object), ((byte[])object).length);
        } else if ((object instanceof Class)) {
          if ("int".equals(contenido)) {
            ps.setNull(i + 1, 4);
          } else if ("long".equals(contenido)) {
            ps.setNull(i + 1, -5);
          } else if ("double".equals(contenido)) {
            ps.setNull(i + 1, 8);
          }
        }
      }
      if (numMaxRegistros > 0) {
        ps.setMaxRows(numMaxRegistros);
      }
      return ps;
    }
    catch (Throwable e)
    {
      throw new AppException("Error al generar la consulta precompilada", e);
    }
  }
  
  public PreparedStatement queryPrecompilado(String query, List lVarBind)
    throws SQLException
  {
    return queryPrecompilado(query, lVarBind, 0);
  }
  
  public PreparedStatement queryPrecompilado(String query)
    throws SQLException
  {
    return queryPrecompilado(query, new ArrayList());
  }
  
  public CallableStatement ejecutaSP(String nombre_sp)
  {
    CallableStatement cstm = null;
    try
    {
      cstm = this.con.prepareCall("{call " + nombre_sp + "}");
      return cstm;
    }
    catch (Exception e)
    {
      throw new AppException("Error al ejecutar el procedimiento", e);
    }
  }
  
  public CallableStatement procesoPrecompilado(String proceso, List lVarBind)
  {
    CallableStatement cs = null;
    try
    {
      cs = ejecutaSP(proceso);
      for (int i = 0; i < lVarBind.size(); i++)
      {
        Object object = lVarBind.get(i);
        String contenido = object.toString().trim();
        if ((object instanceof String)) {
          cs.setString(i + 1, contenido);
        } else if ((object instanceof Integer)) {
          cs.setInt(i + 1, new Integer(contenido).intValue());
        } else if ((object instanceof Long)) {
          cs.setLong(i + 1, new Long(contenido).longValue());
        } else if ((object instanceof Double)) {
          cs.setDouble(i + 1, new Double(contenido).doubleValue());
        } else if ((object instanceof BigDecimal)) {
          cs.setBigDecimal(i + 1, new BigDecimal(contenido));
        } else if ((object instanceof Calendar)) {
          cs.setTimestamp(i + 1, new Timestamp(((Calendar)object).getTime().getTime()), Calendar.getInstance());
        } else if ((object instanceof Date)) {
          cs.setTimestamp(i + 1, new Timestamp(((Date)object).getTime()), Calendar.getInstance());
        } else if ((object instanceof Class)) {
          if ("int".equals(contenido)) {
            cs.setNull(i + 1, 4);
          } else if ("long".equals(contenido)) {
            cs.setNull(i + 1, -5);
          } else if ("double".equals(contenido)) {
            cs.setNull(i + 1, 8);
          }
        }
      }
      return cs;
    }
    catch (Exception e)
    {
      if (cs != null) {
        try
        {
          cs.close();
        }
        catch (Exception ex)
        {
          log.debug("Error al cerrar el CallableStatement");
        }
      }
      log.debug("AccesoDB::procesoPrecompilado(Exception) " + e);
      log.debug("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
      log.debug("qrySentencia:" + proceso);
      log.debug("lVarBind:" + lVarBind);
      log.debug("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
      throw new AppException("Error en procesoPrecompilado", e);
    }
  }
  
  public void terminaTransaccion(boolean todo_ok)
  {
    try
    {
      if (todo_ok)
      {
        this.con.commit();
        log.debug("terminaTransaccion(). Commit");
      }
      else
      {
        this.con.rollback();
        log.debug("terminaTransaccion(). Rollback");
      }
    }
    catch (Exception e)
    {
      if (todo_ok) {
        throw new AppException("Error al terminar la transaccion", e);
      }
      log.debug("Error al terminar la transaccion (rollback): " + e.getMessage());
    }
  }
  
  public Connection getConnection()
  {
    return this.con;
  }
  
  public void cierraConexionDB()
  {
    try
    {
      if (this.con != null) {
        this.con.close();
      }
    }
    catch (Exception e)
    {
      log.debug("Error al cerrar laa conexion: " + e.getMessage());
    }
    finally
    {
      this.con = null;
    }
  }
  
  public boolean hayConexionAbierta()
  {
    return this.con != null;
  }
  
  public Registros consultarDB(String qrySentencia, List lVarBind, boolean bGetDataType, int numMaxRegistros)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;
    ResultSetMetaData rsMD = null;
    List nombres = new ArrayList();
    List renglones = new ArrayList();
    List columnas = null;
    Registros registros = new Registros();
    if (lVarBind == null) {
      lVarBind = new ArrayList();
    }
    try
    {
      ps = queryPrecompilado(qrySentencia, lVarBind, numMaxRegistros);
      rs = ps.executeQuery();
      ps.clearParameters();
      registros = getRegistrosFromRS(rs, bGetDataType);
      if (rs != null) {
        rs.close();
      }
      if (ps != null) {
        ps.close();
      }
    }
    catch (Exception e)
    {
      if (rs != null) {
       // rs.close();
      }
      if (ps != null) {
       // ps.close();
      }
      log.debug("AccesoDB::consultarDB(Exception) " + e);
      log.debug("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
      log.debug("qrySentencia:" + qrySentencia);
      log.debug("lVarBind:" + lVarBind);
      log.debug("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
      e.printStackTrace();
      throw new AppException("Error en consultarDB", e);
    }
    finally {}
    return registros;
  }
  
  public Registros getRegistrosFromRS(ResultSet rs, boolean bGetDataType)
  {
    ResultSetMetaData rsMD = null;
    List nombres = new ArrayList();
    List renglones = new ArrayList();
    List columnas = null;
    Registros registros = new Registros();
    try
    {
      rsMD = rs.getMetaData();
      int numCols = rsMD.getColumnCount();
      int cont = 0;
      while (rs.next())
      {
        columnas = new ArrayList();
        for (int i = 1; i <= numCols; i++)
        {
          if (cont == 0) {
            nombres.add(rsMD.getColumnName(i));
          }
          if (bGetDataType)
          {
            int colType = rsMD.getColumnType(i);
            switch (colType)
            {
            case 2: 
            case 3: 
            case 4: 
              BigDecimal rs_numeric = rs.getBigDecimal(i) == null ? new BigDecimal("0") : rs.getBigDecimal(i);
              

              columnas.add(rs_numeric);
              break;
            case 91: 
            case 92: 
            case 93: 
              Timestamp rs_fecha = rs.getTimestamp(i);
              columnas.add(rs_fecha);
              break;
            default: 
              String rs_campo = rs.getString(i) == null ? "" : rs.getString(i);
              
              columnas.add(rs_campo);
              break;
            }
          }
          else
          {
            String rs_campo = rs.getString(i) == null ? "" : rs.getString(i);
            
            columnas.add(rs_campo);
          }
        }
        renglones.add(columnas);
        cont++;
      }
      registros.setNombreColumnas(nombres);
      registros.setData(renglones);
      return registros;
    }
    catch (Exception e)
    {
      try
      {
        if (rs != null) {
          rs.close();
        }
      }
      catch (Exception t)
      {
        log.debug("AccesoDB.getRegistrosFromRS():: Error al cerrar ResultSet: " + t.getMessage());
      }
      throw new AppException("Error al convertir el ResultSet en objeto Registros", e);
    }
  }
  
  public Registros consultarDB(String qrySentencia, List lVarBind, boolean bGetDataType)
  {
    return consultarDB(qrySentencia, lVarBind, bGetDataType, 0);
  }
  
  public Registros consultarDB(String qrySentencia, List lVarBind)
  {
    return consultarDB(qrySentencia, lVarBind, false);
  }
  
  public Registros consultarDB(String qrySentencia)
  {
    return consultarDB(qrySentencia, new ArrayList());
  }
  
  public int ejecutaUpdateDB(String qrySentencia, List lVarBind)
    throws Exception
  {
    log.debug("AccesoDB::ejecutaUpdateDB(E)");
    PreparedStatement ps = null;
    int registros = 0;
    try
    {
      ps = queryPrecompilado(qrySentencia, lVarBind);
      registros = ps.executeUpdate();
      ps.close();
    }
    catch (Exception e)
    {
      if (ps != null) {
        ps.close();
      }
      log.debug("AccesoDB::ejecutaUpdateDB(Exception) " + e);
      log.debug("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
      log.debug("qrySentencia:" + qrySentencia);
      log.debug("lVarBind:" + lVarBind);
      log.debug("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
      e.printStackTrace();
      throw e;
    }
    finally
    {
      log.debug("AccesoDB::ejecutaUpdateDB(S)");
    }
    return registros;
  }
  
  public int ejecutaUpdateDB(String qrySentencia)
    throws Exception
  {
    return ejecutaUpdateDB(qrySentencia, new ArrayList());
  }
}
