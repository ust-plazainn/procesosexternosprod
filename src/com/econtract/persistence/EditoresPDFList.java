package com.econtract.persistence;

import com.econtract.Comunes;
import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

public class EditoresPDFList
  implements Serializable
{
  private static EditoresPDFList singleton = null;
  private static Properties parametros = null;
  
  public static synchronized EditoresPDFList getInstance()
  {
    if (singleton == null) {
      singleton = new EditoresPDFList();
    }
    return singleton;
  }
  
  public String getParametro(String clave)
  {
    if (parametros == null) {
      try
      {
        parametros = Comunes.loadParams("listParamPDF");
      }
      catch (IOException e)
      {
        parametros = new Properties();
      }
    }
    String valorParametro = parametros.getProperty(clave, "");
    
    return valorParametro;
  }
}
