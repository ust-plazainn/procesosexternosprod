package com.econtract;

import com.econtract.data.ArchivoExcel;
import com.econtract.mail.Correo;
import com.econtract.persistence.AccesoDB;
import java.io.IOException;

public class EnviaInfoProcesoBatch
{
  public static void main(String[] args)
    throws Exception
  {
    AccesoDB con = new AccesoDB();
    AccesoDB econ = new AccesoDB("ECONTRACT");
    boolean exito = false;
    try
    {
      con.conexionDB();
      econ.conexionDB();
      String SQL = "select RFC_PYME,DOC_PROCESADO,DFECHA_CREA_EXPED,DINICIO_DOCTO,DFIN_DOCTO,SIZE_DOC,DESCRIPCION from WWW_BIT_ARMA_EXP                      where  to_char(SYSDATE,'YYYY:MM:DD') = to_char(DINICIO_DOCTO,'YYYY:MM:DD')                          order by RFC_PYME,DFIN_DOCTO ";
      

      String SqlCorreo = "select vvalor as correo from ECON_SYS_PARAMETROS where VNOMBRE_PARAMETRO='EMAIL_SUP_MESA'";
      
      Registros reg = con.consultarDB(SQL);
      Registros regCorreo = econ.consultarDB(SqlCorreo);regCorreo.next();
      

      Correo correo = new Correo();
      correo.setNoResponseSender();
      
      ArchivoExcel arEx = new ArchivoExcel(reg);
      correo.enviaCorreoDatoAdjunto(regCorreo.getString("CORREO"), "Expedientes Procesados ARMA_EXPEDIENTE", "<font style='font-size:9.0pt;font-family:Arial,sans-serif'>Estimado usuario<br><p>A continuaci�n le adjuntamos un archivo el resultado del procesamiento de expedientes digitales del d�a de hoy realizados por el Proceso ARMA_EXPEDIENTE.</p><br><br><br><br><b>Nota: Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos electr�nicos de entrada. Por favor no respondas a este mensaje</b></font>", arEx.getArci());
      







      exito = true;
      System.exit(0);
    }
    catch (IOException e)
    {
      e.printStackTrace();
      System.exit(2);
    }
    catch (Throwable e)
    {
      e.printStackTrace();
      System.exit(2);
    }
    finally
    {
      if (con.hayConexionAbierta())
      {
        con.terminaTransaccion(exito);
        con.cierraConexionDB();
      }
      if (econ.hayConexionAbierta())
      {
        econ.terminaTransaccion(exito);
        econ.cierraConexionDB();
      }
    }
  }
}
