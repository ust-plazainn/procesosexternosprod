package com.econtract;

import java.io.Serializable;
import java.util.ArrayList;

public class DataSet
  implements Serializable
{
  private boolean onError;
  private String errorMessage;
  private int sqlErrorCode;
  private Exception theException;
  private ArrayList arDatos;
  private String strOrderByFields;
  
  public DataSet()
  {
    this.onError = false;
    this.arDatos = new ArrayList();
  }
  
  public int getRowCount()
  {
    return this.arDatos.size();
  }
  
  public void addRow(DataRow row)
  {
    this.arDatos.add(row);
  }
  
  public DataRow getRow(int id)
  {
    return (DataRow)this.arDatos.get(id);
  }
  
  public void removeRow(int id)
  {
    this.arDatos.remove(id);
  }
  
  public String getOrderByFields()
  {
    return this.strOrderByFields;
  }
  
  public void setOrderByFields(String strOrderByFields)
  {
    this.strOrderByFields = strOrderByFields;
  }
  
  public String toJSON()
  {
    String result = "";
    DataRow row = null;
    ArrayList fieldNames = null;
    if (this.arDatos.size() <= 0)
    {
      result = "[]";
    }
    else
    {
      result = "[";
      for (int i = 0; i < this.arDatos.size(); i++)
      {
        row = (DataRow)this.arDatos.get(i);
        if (fieldNames == null) {
          fieldNames = row.getFieldNames();
        }
        result = result + "{";
        for (int j = 0; j < fieldNames.size(); j++)
        {
          result = result + "\"" + fieldNames.get(j) + "\":\"" + row.getData((String)fieldNames.get(j)) + "\"";
          if (j != fieldNames.size() - 1) {
            result = result + ",";
          }
        }
        result = result + ",\"ROWNUM\":" + i + "}";
        if (i != this.arDatos.size() - 1) {
          result = result + ",";
        }
      }
      result = result + "]";
    }
    return result;
  }
  
  public void close()
  {
    for (int i = 0; i < this.arDatos.size(); i++) {
      ((DataRow)this.arDatos.get(i)).close();
    }
    this.arDatos.clear();
    this.arDatos = null;
  }
  
  public void setOnError(boolean onError)
  {
    this.onError = onError;
  }
  
  public boolean isOnError()
  {
    return this.onError;
  }
  
  public void setErrorMessage(String errorMessage)
  {
    this.errorMessage = errorMessage;
  }
  
  public String getErrorMessage()
  {
    return this.errorMessage;
  }
  
  public void setTheException(Exception theException)
  {
    this.theException = theException;
  }
  
  public Exception getTheException()
  {
    return this.theException;
  }
  
  public void setSqlErrorCode(int sqlErrorCode)
  {
    this.sqlErrorCode = sqlErrorCode;
  }
  
  public int getSqlErrorCode()
  {
    return this.sqlErrorCode;
  }
}
